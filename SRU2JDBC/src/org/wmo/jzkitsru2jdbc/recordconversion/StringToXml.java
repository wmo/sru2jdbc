/******************************************************************************
 * Copyright (C) 2010 World Meteorological Organization
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as published by
 * the Free Software Foundation; 
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 * 
 * Contact: Timo Pröscholdt <tproescholdt_at_wmo.int> , World Meteorological Organization
 *
 *******************************************************************************/
package org.wmo.jzkitsru2jdbc.recordconversion;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jzkit.search.util.RecordBuilder.RecordBuilder;
import org.jzkit.search.util.RecordBuilder.RecordBuilderException;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

@Service("org.jzkit.recordbuilder.xml")
public class StringToXml implements RecordBuilder{
	private static Log log = LogFactory.getLog(StringToXml.class);
	
	public StringToXml() {
		log.debug("converter loaded");
	}
	
	@Override
	public Object createFrom(Document arg0, String arg1)
			throws RecordBuilderException {
		throw new RecordBuilderException("not yet implemented");
	}

	@Override
	public Document getCanonicalXML(Object aInput) throws RecordBuilderException {
		DocumentBuilderFactory f = DocumentBuilderFactory.newInstance();
		DocumentBuilder db;
		try {
			db = f.newDocumentBuilder();
			return db.parse(new InputSource(new StringReader((String)aInput)));
		} catch (Exception e) {
			String msg = "converting string to xml ";
			log.error(msg, e);
			throw new RecordBuilderException(msg,e);
		}
	}

}
