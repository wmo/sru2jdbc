/******************************************************************************
 * Copyright (C) 2010 World Meteorological Organization
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as published by
 * the Free Software Foundation; 
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 * 
 * Contact: Timo Pröscholdt <tproescholdt_at_wmo.int> , World Meteorological Organization
 *
 *******************************************************************************/
package org.wmo.jzkitsru2jdbc.servlets;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jzkit.search.LandscapeSpecification;
import org.jzkit.search.SearchSessionFactory;
import org.jzkit.search.StatelessSearchResultsPageDTO;
import org.jzkit.search.landscape.SimpleLandscapeSpecification;
import org.jzkit.search.provider.iface.SearchException;
import org.jzkit.search.util.Profile.ProfileServiceException;
import org.jzkit.search.util.QueryModel.InvalidQueryException;
import org.jzkit.search.util.RecordModel.ArchetypeRecordFormatSpecification;
import org.jzkit.search.util.RecordModel.ExplicitRecordFormatSpecification;
import org.jzkit.search.util.RecordModel.RecordFormatSpecification;
import org.jzkit.search.util.ResultSet.IRResultSetStatus;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.wmo.jzkitsru2jdbc.servlets.diag.SruDiag;
import org.wmo.jzkitsru2jdbc.servlets.diag.SruCheckerResult;
import org.wmo.jzkitsru2jdbc.servlets.diag.SruChecker;
import org.wmo.jzkitsru2jdbc.utils.DefaultContextSetCQLString;
import org.wmo.jzkitsru2jdbc.utils.IndexNotMappedException;
import org.wmo.jzkitsru2jdbc.utils.MaskedWordTooShortException;
import org.wmo.jzkitsru2jdbc.utils.UnsupportedRelationException;
import org.wmo.jzkitsru2jdbc.utils.config.SRU2JDBCConfig;

/**
 * A SRU servlet. For an explanation of SRU requests see http://www.loc.gov/standards/sru/specs
 * 
 * @author timo proescholdt <tproescholdt@wmo.int>
 * @version 0.5
 */
public  class WMOSRU extends HttpServlet {
	
	/** serial version uid. */
    private static final long serialVersionUID = 7538239706122144661L;
    
	public static Log log = LogFactory.getLog(WMOSRU.class);

	private static RecordFormatSpecification request_spec = new ArchetypeRecordFormatSpecification("F");

	/** utility used for parameter testing - will be used accross sessions. */
    private SruChecker sruChecker = new SruChecker();
	
	//-------------------------------------------------------------------------
    //   initialisation
    //-------------------------------------------------------------------------
	/**
	 * constructor
	 */
	public WMOSRU() {
		log.debug("WMOSRU::WMOSRU constructor");
	}

	/**
	 * Servlet init method, calls parents init.
	 * @return void
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		log.debug("SRU Servlet init");
	}

	//-------------------------------------------------------------------------
    //   protocol methods
    //-------------------------------------------------------------------------
	/**
	 * handle HTTP Get method.
	 * @param aRequest HTTP request
	 * @param aResponse HTTP response
	 * @return void
	 */
	public void doGet(HttpServletRequest aRequest, HttpServletResponse aResponse) throws ServletException, IOException {
		log.debug("doGet");
		doPost(aRequest, aResponse);
	}

	/**
     * handle HTTP post method. we check for a valid request and process it. in case of
     * errors we respond with a diagnostic message according to the spec. in all cases 
     * we forward to a jsp for presenting the results.
     * 
     * @param aRequest HTTP request
     * @param aResponse HTTP response
     */
    public void doPost(HttpServletRequest aRequest, 
                       HttpServletResponse aResponse) throws ServletException, IOException {
        log.debug("******************* SRU::doPost **************************");
        // we are producing xml
        aResponse.setContentType("text/xml");
        aRequest.setCharacterEncoding("UTF-8");
        // response must be utf-8 encoded
        aResponse.setCharacterEncoding("UTF-8");
        RequestDispatcher theView = null ;

        try {
            WebApplicationContext theContext = WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());

            // get the operation
            String theOperation = sruChecker.testOperation(aRequest.getParameter(SruChecker.KEYWORD_OPERATION));
            if (theOperation == null) {
                theView = aRequest.getRequestDispatcher("/index.jsp");  
            }
            else {
                log.debug("operation="+theOperation);
    
                // handle the operation.  
                if ( theOperation.toLowerCase().equalsIgnoreCase(SruChecker.OP_SEARCHRETRIEVE) ) {
                    theView = aRequest.getRequestDispatcher("/searchRetrieve.jsp"); 
                    processSearchRetrieve(theContext, aRequest);
                }
                else if (theOperation.toLowerCase().equalsIgnoreCase(SruChecker.OP_SCAN) ) {
                    theView = aRequest.getRequestDispatcher("/scan.jsp");  
                    processScan(theContext, aRequest);
                }
                else  {
                    // an unsupported operation leads to an explain response with embedded diagnostics 
                    theView = aRequest.getRequestDispatcher("/explain.jsp");  
                    processExplain(theContext, aRequest);
                }
            }
        } 
        catch (SearchException se) {
            // if jzkit complains with a search exception it might be an unsupported index 
            theView = processException(se,aRequest,theView, new ExceptionProcessor() {
                
                @Override
                public void processException(Exception aEx, HashSet<SruDiag> aDiags) {
                    if (aEx.getCause() != null && aEx.getCause() instanceof ProfileServiceException) {
                        ProfileServiceException theProfileException = (ProfileServiceException)aEx.getCause();
                        aDiags.add(createDiag(SruChecker.DIAG_UNSUPPORTED_INDEX, 
                                                "Unsupported Index", 
                                                theProfileException.getMessage()));
                    }
                    else {
                        aDiags.add(createDiag(SruChecker.DIAG_QUERY_SYNTAX, 
                                                "Query syntax error", 
                                                aEx.getMessage()));
                    }
                }
            });
        }
        catch (InvalidQueryException ive) {
            // when a query syntax error was reported
            theView = processException(ive,aRequest, theView, new ExceptionProcessor() {
                
                @Override
                public void processException(Exception aEx, HashSet<SruDiag> aDiags) {
                    aDiags.add(createDiag(SruChecker.DIAG_QUERY_SYNTAX, 
                                            "Query syntax error", 
                                            aEx.getMessage()));
                }
            });
        }
        catch (IndexNotMappedException ie) {
            // if we forgot to map an index 
            theView = processException(ie,aRequest, theView, new ExceptionProcessor() {
                
                @Override
                public void processException(Exception aEx, HashSet<SruDiag> aDiags) {
                    aDiags.add(createDiag(SruChecker.DIAG_UNSUPPORTED_INDEX, 
                                            "Unsupported index", 
                                            aEx.getMessage()));
                }
            });
        }
        catch (MaskedWordTooShortException ie) {
            // if a input parameter contains less than 3 Non-wildcard characters 
            theView = processException(ie,aRequest, theView, new ExceptionProcessor() {
                
                @Override
                public void processException(Exception aEx, HashSet<SruDiag> aDiags) {
                    aDiags.add(createDiag(SruChecker.DIAG_MASKED_WORD_TOO_SHORT, 
                                            "Masked words too short", 
                                            aEx.getMessage()));
                }
            });
        }
        catch (UnsupportedRelationException ue) {
            // if we do not support the requested relation 
            theView = processException(ue,aRequest, theView, new ExceptionProcessor() {
                
                @Override
                public void processException(Exception aEx, HashSet<SruDiag> aDiags) {
                    aDiags.add(createDiag(SruChecker.DIAG_UNSUPPORTED_RELATION, 
                                            "Unsupported relation", 
                                            aEx.getMessage()));
                }
            });
        }
        catch (Exception ge) {
            // if we do not know what went wrong 
            theView = processException(ge,aRequest, theView, new ExceptionProcessor() {
                
                @Override
                public void processException(Exception aEx, HashSet<SruDiag> aDiags) {
                    aDiags.add(createDiag(SruChecker.DIAG_GENERAL_ERROR, 
                                            "General system error", 
                                            aEx.getMessage()));
                }
            });
        }
        finally {
            theView.forward(aRequest, aResponse);
            log.debug("WMOSRU::doPost end");
        }
    }

    //-------------------------------------------------------------------------
    //   explain
    //-------------------------------------------------------------------------
    /**
     * do explain .
     * 
     * @param aContext
     *            Spring application context file
     * @param aRequest
     *            HTTP request
     * @return void
     */
    private void processExplain(WebApplicationContext aContext, 
                                HttpServletRequest aRequest) throws ServletException, IOException {
        log.debug("processExplain");
        aRequest.setAttribute("version", SruChecker.DEFAULT_VERSION);
        
        Set<SruDiag> theDiag = checkRequest(SruChecker.OP_EXPLAIN, convertRequestParameters(aRequest) );        
        if ( theDiag != null && theDiag.size() > 0) {
            aRequest.setAttribute("diagnostics", theDiag);
        }

        SRU2JDBCConfig config = (SRU2JDBCConfig)aContext.getBean("SRU2JDBCconfig");

        aRequest.setAttribute("host", aRequest.getServerName());
        aRequest.setAttribute("port", aRequest.getLocalPort());
        aRequest.setAttribute("database", extractDatabase(aRequest));
        aRequest.setAttribute("supportedAttributes", config.getAttributes());
        aRequest.setAttribute("numberOfRecords", SruChecker.DEFAULT_RECORDS_PER_PAGE);
        aRequest.setAttribute("maximumRecords", SruChecker.DEFAULT_MAXRECORDS_PER_PAGE);
        copyStyleSheetSettings(aRequest);
    }
    
    //-------------------------------------------------------------------------
    //   searchretrieve
    //-------------------------------------------------------------------------

    /**
     * Process search retrieve
     * @param aContext Spring application context file
     * @param request HTTP request
     * @return void
     */
    private void processSearchRetrieve(WebApplicationContext aContext, 
                                       HttpServletRequest aRequest) throws Exception{
        log.debug("WMOSRU::processSearchRetrieve");
        
        // in case of problems we must supply the following attributes in the response
        aRequest.setAttribute("version", SruChecker.DEFAULT_VERSION);
        aRequest.setAttribute("numberRecords", new Integer(0));

        // check for parameters
        Hashtable<String, String> theParams = convertRequestParameters(aRequest);
        Set<SruDiag> theDiags = checkRequest(SruChecker.OP_SEARCHRETRIEVE, theParams );        
        if ( theDiags != null && theDiags.size() > 0) {
            aRequest.setAttribute("diagnostics", theDiags);
            return;
        }
        
        String query = sruChecker.getRequestParamByName(SruChecker.OP_SR_QUERY, theParams);
        String start_record = sruChecker.getRequestParamByName(SruChecker.OP_SR_STARTREC, theParams);
        String maximum_records = sruChecker.getRequestParamByName(SruChecker.OP_SR_MAXREC, theParams);
        String record_schema = sruChecker.getRequestParamByName(SruChecker.OP_SR_RECSCHEMA, theParams);
        String baseURL = getBaseURL(aRequest);

        int num_hits_per_page = SruChecker.DEFAULT_RECORDS_PER_PAGE;
        int first_record = 1;

        if ( maximum_records != null )
            num_hits_per_page = Integer.parseInt(maximum_records);

        if (num_hits_per_page > SruChecker.DEFAULT_MAXRECORDS_PER_PAGE ) 
            num_hits_per_page = SruChecker.DEFAULT_MAXRECORDS_PER_PAGE;
        
        if ( start_record != null ) 
            first_record = Integer.parseInt(start_record);


        ExplicitRecordFormatSpecification display_spec = getDisplaySpec(record_schema);

        SearchSessionFactory theSearchSessionFactory = getSearchSessionFactory(aContext);

        LandscapeSpecification collection = new SimpleLandscapeSpecification(SruChecker.COLLECTION);

        DefaultContextSetCQLString model = (DefaultContextSetCQLString)aContext.getBean("CQLModel");
        model.parse(query);

        String theResultSetId = sruChecker.getRequestParamByName(SruChecker.OP_SR_RESULTSETID, theParams);
        if (theResultSetId != null) {
            log.debug("WMOSRU: reusing resultsetid " + theResultSetId);
        }
        StatelessSearchResultsPageDTO result = theSearchSessionFactory.getResultsPageFor(theResultSetId,
                model,
                collection,
                first_record,
                num_hits_per_page,
                request_spec,
                display_spec,
                null);

        log.debug("WMOSRU::Call to getResultsPageFor completed : "+result);

        if (result.getSearchStatus() == IRResultSetStatus.FAILURE ) {
            String errmsg = "Failure during search";
            log.error(errmsg);
            throw new SearchException(errmsg);
        }
        else {
            aRequest.setAttribute(SruChecker.RESP_FIRSTREC, first_record);
            aRequest.setAttribute(SruChecker.OP_SR_STARTREC, "" + start_record);
            aRequest.setAttribute(SruChecker.OP_SR_MAXREC, "" + num_hits_per_page);
            aRequest.setAttribute(SruChecker.RESP_NUMREC, new Integer(result.getRecordCount()));
            aRequest.setAttribute(SruChecker.RESP_RESULTSETID, result.result_set_id);
            aRequest.setAttribute(SruChecker.OP_SR_RECPACK, "xml");
            aRequest.setAttribute(SruChecker.OP_SR_RECSCHEMA, record_schema);
            aRequest.setAttribute(SruChecker.OP_SR_QUERY, query);
            aRequest.setAttribute(SruChecker.RESP_BASEURL, baseURL);
            // seems to be always zero which is not valid according the xsd
            //aRequest.setAttribute("resultSetIdle",result.result_set_idle_time);
            copyStyleSheetSettings(aRequest);
        }

        // it seems to be necessary to convert the objects into strings here in the controller
        // (only do this if results have been provided)
        String[] temp = null;
        int theRecordCount = 0;
        if (result.records != null) {
            theRecordCount = result.records.length;
            temp = new String[theRecordCount];

            for (int i = 0; i < theRecordCount; i++) {
                if (aRequest.getAttribute(SruChecker.OP_SR_STYLESH) != null ) {
                    // if the request contains a stylesheet transformation we need to strip off 
                    // some xml artifacts to get a valid response
                    temp[i] = cleanupXml(result.records[i].getOriginalObject().toString());
                }
                else {
                    // if no stylesheet transformation is given we pack up the xml into a  
                    // CDATA block to get a valid response, but leave the original xml untouched
                    temp[i] = "<![CDATA[" + result.records[i].getOriginalObject().toString() + "]]>";
                }
            }
        }

        aRequest.setAttribute(SruChecker.RESP_RECORDS, result.records);
        aRequest.setAttribute(SruChecker.RESP_RECORDS2, temp);

        final int last_record = first_record + theRecordCount;
        if ((last_record <= result.getRecordCount())) {
            aRequest.setAttribute(SruChecker.RESP_NEXTRECPOS, "" + (last_record));
        }
    }

    /**
     * removes xml artifacts which currently prevent successful validation
     * 
     * @param aXml xml as contained in the clob
     * 
     * @return cleaned up xml
     */
    private String cleanupXml(String aXml) {

        // get rid of xml declaration because of validation
        if (aXml.startsWith("<?xml version=")) {
            aXml = aXml.substring(aXml.indexOf('>') + 1);
        }
        
        // get rid of schemaLocation because of validation
        return cutSchemaLocations(aXml);
    }
    
    /**
     * cuts off all schemaLocation from the given xml. We found out, that the sru response
     * is only valid if the schemaLocation for validation is omitted both in individual records
     * and in the sru header.
     * 
     * if the schemaLocation is given for multiple gmd records validation complains about 
     * Target namespace already used for validation. We cut this off here.
     * 
     * if schemaLocation is given in the searchRetrieve header and the data contain duplicate
     * ids (such as <gmd:MD_DataIdentification id="identInfo">) we get an error as well. (see 
     * serachRetrieve.jsp)
     * 
     * the obvious workaround seems to be to just forget about validation of the recordData
     * 
     * @param aXml the xml 
     * 
     * @return stripped xml or original if no schemaLocations
     */
    private String cutSchemaLocations(String aXml) {
        String theRet = aXml;
        int theIdx = aXml.indexOf("schemaLocation");
        if (theIdx != -1) {
            // find begin of schemaLocation possibly xsi:schemaLocation but we can't be sure
            int i = 1;
            while (true) {
                char c = aXml.charAt(theIdx - i);
                if (Character.isWhitespace(c)) {
                    break;
                }
                i++;
            }
            
            // contains up to xsi:schemaLocation
            theIdx = theIdx - i;
            theRet = aXml.substring(0, theIdx);
            
            // find till the closing "
            int countApos=0;
            for (i=theIdx; i<aXml.length() && countApos < 2; i++) {
                if (aXml.charAt(i) == '"') {
                    countApos++;
                }
            }
            theRet = theRet + aXml.substring(i);
        }
        return theRet;
    }

    //-------------------------------------------------------------------------
    //   scan
    //-------------------------------------------------------------------------
    /**
     * do scan .
     * 
     * @param aContext
     *            Spring application context file
     * @param aRequest
     *            HTTP request
     * @return void
     */
    private void processScan(WebApplicationContext aContext, 
                                HttpServletRequest aRequest) throws ServletException, IOException {
        log.debug("processScan");
        aRequest.setAttribute("version", SruChecker.DEFAULT_VERSION);
        
        Set<SruDiag> theDiag = checkRequest(SruChecker.OP_SCAN, convertRequestParameters(aRequest) );        
        if ( theDiag != null && theDiag.size() > 0) {
            aRequest.setAttribute("diagnostics", theDiag);
        }
        copyStyleSheetSettings(aRequest);
    }
    
    //-------------------------------------------------------------------------
    //   helper methods
    //-------------------------------------------------------------------------
    /**
     * extract the collection from the URL
     * 
     * @param aRequest
     * @return
     */
    private String extractDatabase(HttpServletRequest aRequest) {
        
        String theIdentifier = aRequest.getContextPath();
        if (theIdentifier != null) {
            theIdentifier += aRequest.getServletPath();
        }
        else {
            theIdentifier = SruChecker.COLLECTION;
        }
        if ((theIdentifier != null) && (theIdentifier.startsWith("/"))) {
            theIdentifier = theIdentifier.substring(1, theIdentifier.length());
        }
        return theIdentifier;
        
    }

	/**
	 * determine DisplayRecordFromat 
	 * @param aRecordSchema
	 * @return
	 */
	private ExplicitRecordFormatSpecification getDisplaySpec(String aRecordSchema) {

		if ( aRecordSchema == null || aRecordSchema.length() == 0) 
			aRecordSchema = "ISO19139";

		return new ExplicitRecordFormatSpecification("xml:"+aRecordSchema+":F");

	}
	
    /**
     * get the requests parameters as a hashtable.
     * @param aRequest the request
     * @return all parameters with paramname as key and param value as value
     */
    private Hashtable<String, String> convertRequestParameters(HttpServletRequest aRequest) {

        Hashtable<String, String> theResult = new Hashtable<String, String>();

        for ( Enumeration<String> theParamNames = aRequest.getParameterNames(); theParamNames.hasMoreElements(); ) { 
            String theKey = theParamNames.nextElement();
            String theValue = aRequest.getParameter(theKey);
            theResult.put(theKey, theValue);
        }
        
        return theResult;
    }

    /**
     * @param aContext
     *            the spring application context
     * 
     * @return SearchSessionFactory from spring configuration
     */
    private SearchSessionFactory getSearchSessionFactory(WebApplicationContext aContext) {
        SearchSessionFactory theSearchSessionFactory =
                  (SearchSessionFactory)aContext.getBean("SearchSessionFactory");
        log.debug("WMOSRU::Obtained searchsessionfactory: " + theSearchSessionFactory);
        return theSearchSessionFactory;
    }	

    /**
     * determine the baseURL for this request.
     * @param aRequest the Http Request
     * @return String rep of the URL
     */
    private String getBaseURL(HttpServletRequest aRequest)  {
        String scheme = aRequest.getScheme();
        String server = aRequest.getServerName();
        int port = aRequest.getServerPort();
        String context = getContext(aRequest);
        try {
            URL reconstructedURL = new URL ( scheme, server, port, context );
            return reconstructedURL.toExternalForm();
        }
        catch (MalformedURLException e) {
            log.error("cannot construct url via URL(" + scheme + ", " + server + ", " + port + ", " + context + ")", e);
        }

        return null;
    }
	
    /**
     * @param aRequest the HttpRequest
     * @return context for getting the base url
     */
    private String getContext(HttpServletRequest aRequest)  {
        String theIdentifier = aRequest.getContextPath();
        if (theIdentifier != null && !theIdentifier.endsWith("/")) {
            theIdentifier += "/";
        }
        return theIdentifier;
    }
    

    /**
     * copy stylesheetsettings to request attributes
     * 
     * @param aRequest request object
     */
    private void copyStyleSheetSettings(HttpServletRequest aRequest) {
        copyParamToAttr(aRequest, SruChecker.OP_SR_STYLESH);
        copyParamToAttr(aRequest, SruChecker.OP_SR_STYLESHEET_DETAIL);
    }

    /**
     * copy request parameter to request attribute if the parameter with the given name is supplied.
     * @param aRequest request object
     * @param aParamName the parameter name
     * 
     * @return true if the param was copied
     */
    private boolean copyParamToAttr(HttpServletRequest aRequest, String aParamName) {
        
        String theParamValue = aRequest.getParameter(aParamName);

        if ( ( theParamValue != null ) && ( theParamValue.length() > 0 ) ) {
            aRequest.setAttribute(aParamName, theParamValue ) ;
            return true;
        }
        return false;
    }

    
    //-------------------------------------------------------------------------
    //   diagnostic and exception handling
    //-------------------------------------------------------------------------

    /**
     * check the incoming request for completeness.
     * 
     * @param aOperation the requested operation
     * @param aRequestParameters the requests parameters as key/value pairs
     * 
     * @return set of SruDiag containing diagnostic messages. An empty set indicated no diagnostics.
     *        
     */
    private Set<SruDiag> checkRequest(String aOperation, Hashtable<String, String> aRequestParameters) {

        HashSet<SruDiag> theDiags = new HashSet<SruDiag>();

        SruCheckerResult res = sruChecker.testParams(aOperation, aRequestParameters);

        for (String param : res.getNotSupported()) {
            theDiags.add(createDiag(SruChecker.DIAG_UNSUPPORTED_PARAMETER, 
                                "Unsupported Parameter", 
                                param));
        }

        for (String param : res.getCannotParse()) {
            theDiags.add(createDiag(SruChecker.DIAG_UNSUPPORTED_PARAMETER_VALUE, 
                                "Unsupported parameter value for " + param, 
                                aRequestParameters.get(param)));
        }

        for (String param : res.getMissingArgs()) {
            theDiags.add(createDiag(SruChecker.DIAG_MISSING_PARAMETER, 
                                "Mandatory parameter not supplied", 
                                param));
        }

        for (String param : res.getRecordFailures()) {
            theDiags.add(createDiag(SruChecker.DIAG_PARAMETER_OUTOFRANGE, 
                                "First record position out of range", 
                                param));
        }

        String version = aRequestParameters.get(SruChecker.OP_SR_VERSION);
        if (version != null && (!(version.equals("1.1") || version.equals("1.2")))) {
            theDiags.add(createDiag(SruChecker.DIAG_UNSUPPORTED_VERSION, 
                                "Unsupported version", 
                                "I got version: " + version + " but I support only 1.1 or 1.2"));
        }

        if (!aOperation.equalsIgnoreCase(SruChecker.OP_SEARCHRETRIEVE) && !aOperation.equalsIgnoreCase(SruChecker.OP_EXPLAIN)) {
            theDiags.add(createDiag(SruChecker.DIAG_UNSUPPORTED_OPERATION, 
                                "Unsupported operation", 
                                aOperation));
        }
        return theDiags;
    }
    
    /**
     * creates a new Srudiag from the parameters.
     * @param aUri
     * @param aDetails
     * @param aMessage
     * @return the new SruDiag
     */
    private SruDiag createDiag(String aUri, String aDetails, String aMessage) {
        return new SruDiag(aUri, aDetails, aMessage);
    }

    /**
     * interface to process a exception. 
     */
    interface ExceptionProcessor {
        /**
         * process a exception. it is assumed that the processor adds diagonstics to aDiags.
         * 
         * @param aEx the exception to process
         * @param aDiags the diags 
         */
        public void processException(Exception aEx, HashSet<SruDiag> aDiags);
    }
    
    /**
     * processes an exception that was thrown during an sru request.
     * 
     * @param aEx exception that was thrown
     * @param aRequest the request 
     * @param aView dispatcher for the view - if null we forward to error.jsp
     * @param aProcessor the processor for this exception
     * 
     * @return a dispatcher for the view
     */
    private RequestDispatcher processException(Exception aEx, 
                                               HttpServletRequest aRequest, 
                                               RequestDispatcher aView,
                                               ExceptionProcessor aProcessor) {
        log.error("Exception caught", aEx);

        if ( aView == null) {
            aView = aRequest.getRequestDispatcher("/error.jsp");
        }
        
        HashSet<SruDiag> theDiags = new HashSet<SruDiag>();
        
        aProcessor.processException(aEx, theDiags);
        
        aRequest.setAttribute("diagnostics", theDiags);
        
        return aView;
    }
}
