package org.wmo.jzkitsru2jdbc.servlets.diag;

/**
 * Diagnostic info for sru-search, 
 */
public class SruDiag {
    /** url. */
    private String url;
    
    /** details of this diag. */
    private String details;
    
    /** message of this diag. */
    private String message;

    //-------------------------------------------------------------------------
    //   constructor
    //-------------------------------------------------------------------------
    public SruDiag(String url, String details, String message) {
        super();
        this.url = url;
        this.details = details;
        this.message = message;
    }

    //-------------------------------------------------------------------------
    //   getter/setter
    //-------------------------------------------------------------------------
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
