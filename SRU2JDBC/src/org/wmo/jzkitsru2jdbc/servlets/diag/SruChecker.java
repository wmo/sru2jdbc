package org.wmo.jzkitsru2jdbc.servlets.diag;

import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;

/**
 * utility to test sru requests against the supported configuration 
 * and the compliance to the spec as far as we see use for it.
 * 
 * 
 */
public class SruChecker {
    /** all arguments for explain. */
    private Set<String> explainArgs;

    /** mandatory arguments for explain. */
    private Set<String> explainMandatoryArgs;

    /** all arguments for searchretrieve. */
    private Set<String> searchRetrieveArgs;

    /** mandatory arguments for searchretrieve. */
    private Set<String> searchRetrieveMandatoryArgs;

    /** parameter types. */
    private Hashtable<String, String> paramTypes;

    // -------------------------------------------------------------------------
    // public constants 
    // -------------------------------------------------------------------------

    public static final String KEYWORD_OPERATION = "operation";
    public static final String OP_SEARCHRETRIEVE = "searchRetrieve";
    public static final String OP_EXPLAIN = "explain";
    public static final String OP_SCAN = "scan";
    
    // search and retrieve related constants for request and response.
    public static final String OP_SR_QUERY = "query";
    public static final String OP_SR_VERSION = "version";
    public static final String OP_SR_STYLESH = "stylesheet";
    public static final String OP_SR_STYLESHEET_DETAIL = "x-dwd-stylesheetDetailLevel";
    public static final String OP_SR_RESULTSETID = "x-dwd-resultSetId";
    public static final String OP_SR_LANG = "x-dwd-lang";
    public static final String OP_SR_STARTREC = "startRecord";
    public static final String OP_SR_MAXREC = "maximumRecords";
    public static final String OP_SR_RECPACK = "recordPacking";
    public static final String OP_SR_RECSCHEMA = "recordSchema";
    public static final String OP_SR_RECXPATH = "recordXPath ";
    public static final String OP_SR_SORTKEYS = "sortKeys";
    public static final String OP_SR_EXTRADATA = "extraRequestData";
    

    // to be included in the response only.
    public static final String RESP_FIRSTREC = "firstRecord";
    public static final String RESP_NUMREC = "numberRecords";
    public static final String RESP_NEXTRECPOS = "nextRecordPosition";
    public static final String RESP_BASEURL = "baseUrl";
    public static final String RESP_RECORDS = "records";
    public static final String RESP_RECORDS2 = "records2";
    public static final String RESP_RESULTSETID = "resultSetId";
    

    /** sru version we support */
    public static final String DEFAULT_VERSION = "1.1";
    
    // default record settings.
    public static final int DEFAULT_RECORDS_PER_PAGE = 15;
    public static final int DEFAULT_MAXRECORDS_PER_PAGE = 25;
    
    /** collection code for jzkit config. */
    public static final String COLLECTION = "Default";
    
    // diagnostic constants @see http://www.loc.gov/standards/sru/resources/diagnostics-list.html
    public static final String DIAG_GENERAL_ERROR = "info:srw/diagnostic/1/1";
    public static final String DIAG_UNSUPPORTED_OPERATION = "info:srw/diagnostic/1/4";
    public static final String DIAG_UNSUPPORTED_VERSION = "info:srw/diagnostic/1/5";
    public static final String DIAG_UNSUPPORTED_PARAMETER_VALUE = "info:srw/diagnostic/1/6";
    public static final String DIAG_MISSING_PARAMETER = "info:srw/diagnostic/1/7";
    public static final String DIAG_UNSUPPORTED_PARAMETER = "info:srw/diagnostic/1/8";
    
    public static final String DIAG_QUERY_SYNTAX = "info:srw/diagnostic/1/10";
    public static final String DIAG_UNSUPPORTED_INDEX = "info:srw/diagnostic/1/16";
    public static final String DIAG_UNSUPPORTED_RELATION = "info:srw/diagnostic/1/19";
    public static final String DIAG_UNSUPPORTED_RELATION_MODIFIER = "info:srw/diagnostic/1/20";
    public static final String DIAG_MASKED_WORD_TOO_SHORT = "info:srw/diagnostic/1/29";
    
    public static final String DIAG_PARAMETER_OUTOFRANGE = "info:srw/diagnostic/1/61";

    // -------------------------------------------------------------------------
    // constructors
    // -------------------------------------------------------------------------
    /**
     * constructs a new parameter tester object.
     */
    public SruChecker() {

        explainMandatoryArgs = new HashSet<String>();

        explainMandatoryArgs.add(OP_SR_VERSION.toLowerCase());
        explainMandatoryArgs.add(KEYWORD_OPERATION.toLowerCase());

        explainArgs = new HashSet<String>();

        explainArgs.add(OP_SR_RECPACK.toLowerCase());
        explainArgs.add(OP_SR_STYLESH.toLowerCase());
        explainArgs.add(OP_SR_EXTRADATA.toLowerCase());

        searchRetrieveMandatoryArgs = new HashSet<String>();

        searchRetrieveMandatoryArgs.add(KEYWORD_OPERATION.toLowerCase());
        searchRetrieveMandatoryArgs.add(OP_SR_VERSION.toLowerCase());
        searchRetrieveMandatoryArgs.add(OP_SR_QUERY.toLowerCase());

        searchRetrieveArgs = new HashSet<String>();

        searchRetrieveArgs.add(OP_SR_STARTREC.toLowerCase());
        searchRetrieveArgs.add(OP_SR_MAXREC.toLowerCase());
        searchRetrieveArgs.add(OP_SR_RECPACK.toLowerCase());
        searchRetrieveArgs.add(OP_SR_RECSCHEMA.toLowerCase());
        searchRetrieveArgs.add(OP_SR_RECXPATH.toLowerCase());
        searchRetrieveArgs.add("resultsetttl");
        searchRetrieveArgs.add(OP_SR_SORTKEYS.toLowerCase());
        searchRetrieveArgs.add(OP_SR_STYLESH.toLowerCase());
        searchRetrieveArgs.add(OP_SR_STYLESHEET_DETAIL.toLowerCase());
        searchRetrieveArgs.add(OP_SR_RESULTSETID.toLowerCase());
        searchRetrieveArgs.add(OP_SR_LANG.toLowerCase());
        searchRetrieveArgs.add(OP_SR_EXTRADATA.toLowerCase());

        paramTypes = new Hashtable<String, String>();

        paramTypes.put(OP_SR_QUERY.toLowerCase(), "string");
        paramTypes.put(OP_SR_VERSION.toLowerCase(), "string");
        paramTypes.put(OP_SR_STYLESH.toLowerCase(), "string");
        paramTypes.put(OP_SR_STYLESHEET_DETAIL.toLowerCase(), "string");
        paramTypes.put(OP_SR_STARTREC.toLowerCase(), "int");
        paramTypes.put(OP_SR_MAXREC.toLowerCase(), "int");
        paramTypes.put(OP_SR_RECPACK.toLowerCase(), "string");
        paramTypes.put(OP_SR_RECSCHEMA.toLowerCase(), "string");
        paramTypes.put(OP_SR_RECXPATH.toLowerCase(), "string");
        paramTypes.put(OP_SR_SORTKEYS.toLowerCase(), "string");
        paramTypes.put(OP_SR_EXTRADATA.toLowerCase(), "string");

    }

    // -------------------------------------------------------------------------
    // public methods
    // -------------------------------------------------------------------------
    /**
     * test valid operations.
     * 
     * @return a supported operation
     */
    public String testOperation(String op) {

        String theDefaultOp = null;
        
        if ( op != null && op.length() > 0) {
            if ( op.equalsIgnoreCase(OP_SEARCHRETRIEVE))
                return OP_SEARCHRETRIEVE;
            if ( op.equalsIgnoreCase(OP_EXPLAIN))
                return OP_EXPLAIN;
            if ( op.equalsIgnoreCase(OP_SCAN))
                return OP_SCAN;
        }
        
        return theDefaultOp;
    }
    
    /**
     * test parameters for a operation.
     * 
     * @return test result containing not supported, missing and invalid params
     */
    public SruCheckerResult testParams(String op, Hashtable<String, String> params) {

        Set<String> notSupported = testNotSupported(op, params);
        Set<String> missingArgs = testMissingArgs(op, params);
        Set<String> cannotParse = testCannotParse(op, params);
        Set<String> recordFailures = testRecordFailures( op, params);

        return new SruCheckerResult(op, notSupported, missingArgs, cannotParse, recordFailures);
    }
    
    /**
     * get a request parameter by name ignoring case.
     * 
     * @param aName the parameters name
     * @param aParams the requests parameters
     * 
     * @return the parameter value - null if not present
     */
    public String getRequestParamByName(String aName, Hashtable<String, String> aParams) {
        Set<String> theKeys = aParams.keySet();
        for ( String theKey : theKeys) {
            if (theKey.equalsIgnoreCase(aName)) {
                return aParams.get(theKey);
            }
        }
        return null;
    }

    // -------------------------------------------------------------------------
    // utility methods
    // -------------------------------------------------------------------------
    /**
     * test for not supported arguments for a operation.
     * 
     * @param op
     *            the operation to test
     * @param params
     *            the params given by the user
     * 
     * @return set of unsupported arguments
     */
    private Set<String> testNotSupported(String op, Hashtable<String, String> params) {

        HashSet<String> temp = new HashSet<String>();
        HashSet<String> ret = new HashSet<String>();

        if (op.equalsIgnoreCase(OP_EXPLAIN)) {
            temp.addAll(explainArgs);
            temp.addAll(explainMandatoryArgs);
        }
        else if (op.equalsIgnoreCase(OP_SEARCHRETRIEVE)) {
            temp.addAll(searchRetrieveArgs);
            temp.addAll(searchRetrieveMandatoryArgs);
        }
        else {
            return ret;
        }

        for (String param : params.keySet()) {
            if (!temp.contains(param.toLowerCase())) {
                ret.add(param);
            }
        }

        return ret;
    }

    /**
     * test for missing arguments for a operation.
     * 
     * @param op
     *            the operation to test
     * @param params
     *            the params given by the user
     * 
     * @return set of missing arguments
     */
    private Set<String> testMissingArgs(String op, Hashtable<String, String> params) {

        HashSet<String> temp = new HashSet<String>();
        HashSet<String> ret = new HashSet<String>();

        // add all mandatory args to temp. set
        if (op.equalsIgnoreCase(OP_EXPLAIN)) {
            temp.addAll(explainMandatoryArgs);
        }
        if (op.equalsIgnoreCase(OP_SEARCHRETRIEVE)) {
            temp.addAll(searchRetrieveMandatoryArgs);
        }

        // iterate over params and remove all items from temp which are found there
        Set<String> theKeys = params.keySet();
        for ( String theKey : theKeys) {
            String theValue = params.get(theKey);
            theKey = theKey.toLowerCase();
            if (temp.contains(theKey) && theValue != null && theValue.length() > 0) {
                temp.remove(theKey);
            }
        }

        // remaining entries in temp are missing
        if (temp.size() > 0) {
            ret.addAll(temp);
        }
        return ret;
    }

    /**
     * test for unparseable arguments for a operation.
     * 
     * @param op
     *            the operation to test
     * @param params
     *            the params given by the user
     * 
     * @return set of missing arguments
     */
    private Set<String> testCannotParse(String op, Hashtable<String, String> params) {

        HashSet<String> ret = new HashSet<String>();

        if (op.equalsIgnoreCase(OP_SEARCHRETRIEVE)) {

            for (String key : params.keySet()) {
                String val = params.get(key);
                String keyLc = key.toLowerCase();
                if (paramTypes.containsKey(keyLc) && paramTypes.get(keyLc).equals("int")) {
                    try {
                        Integer.parseInt(val);
                    }
                    catch (NumberFormatException e) {
                        ret.add(key);
                    }
                }
            }

        }
        return ret;
    }

    /**
     * test record failures.
     * 
     * @param aOp
     *            the operation to test
     * @param aParams
     *            the params given by the user
     * 
     * @return set of record failures
     */
    private Set<String> testRecordFailures(String aOp, Hashtable<String, String> aParams) {

        HashSet<String> ret = new HashSet<String>();

// commented this out, as "maximumrecords" must be interpreted as the maximum number of records to
// return in the resultset and not as the maximum record number as it was done here
//        
//        String theMaxRec = getRequestParamByName(OP_SR_MAXREC, aParams);
//        String theStartRec = getRequestParamByName(OP_SR_STARTREC, aParams);
//        if ( theMaxRec != null && theMaxRec.length() > 0 && theStartRec != null && theStartRec.length() > 0) {
//            try {
//                int theMaxRecInt = Integer.parseInt(theMaxRec);
//                int theStartRecInt = Integer.parseInt(theStartRec);
//                if (theStartRecInt > theMaxRecInt) {
//                    ret.add(OP_SR_STARTREC);
//                }
//            } catch(NumberFormatException theNfe) {
//                ; // already handled by testCannotParse()
//            }
//        }
        return ret;
    }
}
