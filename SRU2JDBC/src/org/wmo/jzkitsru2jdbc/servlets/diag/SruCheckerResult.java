package org.wmo.jzkitsru2jdbc.servlets.diag;

import java.util.Set;

/**
 * utility that contains the results of a sru param test.
 */
public class SruCheckerResult {
    /** operation that was tested. */
    private String op;
    
    /** set of not supported arguments. */
    private Set<String> notSupported;

    /** set of missing arguments. */
    private Set<String> missingArgs;

    /** set of unparseable arguments. */
    private Set<String> cannotParse ;

    /** set of record failures. */
    private Set<String> recordFailures ;
    
    //-------------------------------------------------------------------------
    //   constructors
    //-------------------------------------------------------------------------
    /**
     * construct a new SruParamTestResult
     */
    public SruCheckerResult(String aOperation, 
                            Set<String> aNotSupported, 
                            Set<String> aMissingArgs, 
                            Set<String> aCannotParse,
                            Set<String> aRecordFailures
                            ) {
        super();
        this.op = aOperation;
        this.notSupported = aNotSupported;
        this.missingArgs = aMissingArgs;
        this.cannotParse = aCannotParse;
        this.recordFailures = aRecordFailures;
    }

    
    
    //-------------------------------------------------------------------------
    //   getter/setter
    //-------------------------------------------------------------------------
    
    public String getOp() {
        return op;
    }
    
    public void setOp(String op) {
        this.op = op;
    }

    public Set<String> getNotSupported() {
        return notSupported;
    }

    public void setNotSupported(Set<String> notSupported) {
        this.notSupported = notSupported;
    }

    public Set<String> getMissingArgs() {
        return missingArgs;
    }

    public void setMissingArgs(Set<String> missingArgs) {
        this.missingArgs = missingArgs;
    }

    public Set<String> getCannotParse() {
        return cannotParse;
    }

    public void setCannotParse(Set<String> cannotParse) {
        this.cannotParse = cannotParse;
    }

    public Set<String> getRecordFailures() {
        return recordFailures;
    }

    public void setRecordFailures(Set<String> recordFailures) {
        this.recordFailures = recordFailures;
    }
    
}
