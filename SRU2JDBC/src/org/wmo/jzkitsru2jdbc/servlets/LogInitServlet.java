package org.wmo.jzkitsru2jdbc.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 * Log4j Initialization Servlet.
 * 
 */
public class LogInitServlet extends HttpServlet {
    /** generated serial. */
    private static final long serialVersionUID = 9086977521410814022L;

    /** Our logger instance. */
    private static final Logger LOG = Logger.getLogger(LogInitServlet.class);

    /** Default Constructor. */
    public LogInitServlet() {

    }

//    public void init() {
//        final String prefix = getServletContext().getRealPath("/");
//        final String cfg = prefix + "WEB-INF/log4j.properties";
//        System.out.println("YYYYYYYYYYYY " + cfg);
//        PropertyConfigurator.configure(cfg);
//    }
    
    /**
     * @param aRequest the http request
     * @param aResponse the http response
     * @throws ServletException servlet exception occured
     * @throws IOException io exception occured
     * @see javax.servlet.http.HttpServlet#doGet(HttpServletRequest, HttpServletResponse)
     */
    protected void doGet(HttpServletRequest aRequest, HttpServletResponse aResponse) throws ServletException,
                                                                                    IOException {
    }
}