/******************************************************************************
 * Copyright (C) 2010 World Meteorological Organization
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as published by
 * the Free Software Foundation; 
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 * 
 * Contact: Timo Pröscholdt <tproescholdt_at_wmo.int> , World Meteorological Organization
 *
 *******************************************************************************/
package org.wmo.jzkitsru2jdbc.utils;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import java.util.Stack;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jzkit.search.util.QueryModel.InvalidQueryException;
import org.jzkit.search.util.QueryModel.QueryModel;
import org.jzkit.search.util.QueryModel.Internal.AttrPlusTermNode;
import org.jzkit.search.util.QueryModel.Internal.ComplexNode;
import org.jzkit.search.util.QueryModel.Internal.InternalModelRootNode;
import org.jzkit.search.util.QueryModel.Internal.QueryNodeVisitor;
import org.springframework.context.ApplicationContext;
import org.wmo.jzkitsru2jdbc.utils.Nodes.AttributeQueryNode;
import org.wmo.jzkitsru2jdbc.utils.Nodes.BaseNode;
import org.wmo.jzkitsru2jdbc.utils.Nodes.SelectNode;

/**
 * class to decode cql queries.
 *
 */
public class JDBCQueryDecoder
{
	private String attrSet;

	private static Log log = LogFactory.getLog(JDBCQueryDecoder.class);

	private  Query2SQLMapper mapper = null;
	private InternalModelRootNode rn;

	private SelectNode returnnode = null;

	public JDBCQueryDecoder(QueryModel qm, ApplicationContext ctx)
	{
		mapper = (Query2SQLMapper)ctx.getBean("SRU2JDBCconfig"); 

		try
		{
			rn  = qm.toInternalQueryModel(ctx);
		}
		catch (InvalidQueryException iqe)
		{
			iqe.printStackTrace();
		}
	}

	/**
	 * decodes a cql query by visiting all cql-nodes of the rootnode.
	 * 
	 * @return SelectNode which encapsulates the sq query
	 */
	public SelectNode decodeQuery() {

		final Stack<String> stack = new Stack<String>();
		final Set<String> tables = new HashSet<String>();
		final LinkedList<String> parameters = new LinkedList<String>();
        final LinkedList<String> parameterTypes = new LinkedList<String>();

		QueryNodeVisitor qnv = new QueryNodeVisitor()
		{
			public void visit(AttrPlusTermNode aptn) 
			{
				super.visit(aptn);
				AttributeQueryNode node = mapper.map(aptn);
				tables.addAll(node.getTables());
				stack.push(node.getPayLoad());	
				parameters.addAll(node.getQueryFragment().getParameters());
				parameterTypes.addAll(node.getQueryFragment().getParameterTypes());
			}

			public void visit(ComplexNode cn)
			{
				super.visit(cn);

				String right = stack.pop();
				String left = stack.pop();

				BaseNode node = mapper.map(cn);

				String combinedOp = "("+ left + " " + node.getPayLoad() + " " + right +")";

				stack.push(combinedOp);
			}

			public void visit(InternalModelRootNode rn)
			{
				super.visit(rn);
				returnnode = mapper.map(rn);
				tables.addAll(returnnode.getTables());

				String almostQuery = stack.pop();

				returnnode.setAlmostQuery(almostQuery);
				returnnode.setTables(tables);
				returnnode.setParameters(parameters);
				returnnode.setParameterTypes(parameterTypes);
			}

			public void onAttrPlusTermNode(AttrPlusTermNode aptn) {
				log.debug("doing nothing...."+aptn); 
			}

		};
		qnv.visit(rn);

		return returnnode;
	}

	public String getAttrSet()
	{
		return attrSet;
	}
}
