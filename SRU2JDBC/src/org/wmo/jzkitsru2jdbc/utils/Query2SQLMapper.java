/******************************************************************************
 * Copyright (C) 2010 World Meteorological Organization
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as published by
 * the Free Software Foundation; 
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 * 
 * Contact: Timo Pröscholdt <tproescholdt_at_wmo.int> , World Meteorological Organization
 *
 *******************************************************************************/
package org.wmo.jzkitsru2jdbc.utils;

import org.jzkit.search.util.QueryModel.Internal.AttrPlusTermNode;
import org.jzkit.search.util.QueryModel.Internal.ComplexNode;
import org.jzkit.search.util.QueryModel.Internal.InternalModelRootNode;
import org.wmo.jzkitsru2jdbc.utils.Nodes.AttributeQueryNode;
import org.wmo.jzkitsru2jdbc.utils.Nodes.BaseNode;
import org.wmo.jzkitsru2jdbc.utils.Nodes.SelectNode;

public interface Query2SQLMapper {

	public abstract AttributeQueryNode map(AttrPlusTermNode aptn) ;

	public abstract BaseNode map(ComplexNode cnode) ;

	public abstract SelectNode map(InternalModelRootNode root) ;
	

}