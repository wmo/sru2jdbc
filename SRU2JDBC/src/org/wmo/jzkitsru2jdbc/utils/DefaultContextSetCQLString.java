/******************************************************************************
 * Copyright (C) 2010 World Meteorological Organization
 * Copyright (C) 2009 Ian Ibbotson
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as published by
 * the Free Software Foundation; 
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 * 
 * Contact: Timo Pröscholdt <tproescholdt_at_wmo.int> , World Meteorological Organization
 *
 *******************************************************************************/
package org.wmo.jzkitsru2jdbc.utils;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jzkit.search.util.QueryModel.InvalidQueryException;
import org.jzkit.search.util.QueryModel.QueryModel;
import org.jzkit.search.util.QueryModel.Internal.AttrPlusTermNode;
import org.jzkit.search.util.QueryModel.Internal.AttrValue;
import org.jzkit.search.util.QueryModel.Internal.ComplexNode;
import org.jzkit.search.util.QueryModel.Internal.InternalModelNamespaceNode;
import org.jzkit.search.util.QueryModel.Internal.InternalModelRootNode;
import org.jzkit.search.util.QueryModel.Internal.QueryNode;
import org.springframework.context.ApplicationContext;
import org.z3950.zing.cql.CQLAndNode;
import org.z3950.zing.cql.CQLBooleanNode;
import org.z3950.zing.cql.CQLNode;
import org.z3950.zing.cql.CQLNotNode;
import org.z3950.zing.cql.CQLOrNode;
import org.z3950.zing.cql.CQLParser;
import org.z3950.zing.cql.CQLPrefixNode;
import org.z3950.zing.cql.CQLProxNode;
import org.z3950.zing.cql.CQLRelation;
import org.z3950.zing.cql.CQLTermNode;


/**
 * @author 'Timo Proescholdt <tproescholdt@wmo.int>'
 * @author Ian Ibbotson
 * adds default namespaces to Attributes, Relations and Structures
 * parses CQL string and converts it into JZKit internal Query Model
 */
public class DefaultContextSetCQLString implements QueryModel, java.io.Serializable {

	private Log log = LogFactory.getLog(DefaultContextSetCQLString.class);
	private String defAttrNamespace;
	private String defRelNamespace;
	private String defStructNamespace;
	private boolean force_def = false;
	
	private static String default_qualifier="cql.serverChoice";

	private InternalModelRootNode internal_model = null;
	private CQLNode cql_root;

	
	public void parse(CQLNode cql_node) {
		this.cql_root = cql_node;
	}
	
	public void parse(String cql_string) {
		CQLNode node = null;
		try {
			CQLParser parser = new CQLParser();
			node = parser.parse(cql_string);
			log.debug("Parsed CQL");
		}
		catch (  org.z3950.zing.cql.CQLParseException cqle ) {
			log.warn("Problem parsing CQL",cqle);
			// cqle.printStackTrace();
		}
		catch ( java.io.IOException ioe ) {
			log.warn("Problem parsing CQL",ioe);
			// ioe.printStackTrace();
		}
		
		parse(node);
		
	}


	/**
	 * @param force
	 * force the overwriting of attribute and relation default context sets
	 */
	public void setForceContextSet(boolean force) {
		this.force_def = force;
	}



	public InternalModelRootNode toInternalQueryModel(ApplicationContext ctx) throws InvalidQueryException {
		
		if (cql_root == null)
			throw new InvalidQueryException("CQLQuery not yet set. Call parse first");
		
		if ( internal_model == null ) {
			internal_model = new InternalModelRootNode(translate(cql_root));
		}
		return internal_model;
	}

	private QueryNode translate(CQLNode cql_node) {
		QueryNode result = null;

		if ( cql_node instanceof CQLBooleanNode ) {
			CQLBooleanNode cbn = (CQLBooleanNode)cql_node;
			if ( cbn instanceof CQLAndNode ) {
				result = new ComplexNode(translate(cbn.left), translate(cbn.right),ComplexNode.COMPLEX_AND);
			}
			else if ( cbn instanceof CQLOrNode ) {
				result = new ComplexNode(translate(cbn.left),translate(cbn.right),ComplexNode.COMPLEX_OR);
			}
			else if ( cbn instanceof CQLNotNode ) {
				result = new ComplexNode(translate(cbn.left),translate(cbn.right),ComplexNode.COMPLEX_ANDNOT);
			}
			else if ( cbn instanceof CQLProxNode ) {
				result = new ComplexNode(translate(cbn.left),translate(cbn.right),ComplexNode.COMPLEX_PROX);
			}
		}
		else if ( cql_node instanceof CQLTermNode ) {
			log.debug("Warning: We should properly translate the CQLTermNode");
			CQLTermNode  cql_term_node = (CQLTermNode) cql_node;
			AttrPlusTermNode aptn = new AttrPlusTermNode();


			processCQLTermNode(aptn,cql_term_node);
			fixDefaultNamespaces(aptn,cql_term_node);

			result = aptn;
		}
		else if ( cql_node instanceof CQLPrefixNode ) {
			CQLPrefixNode pn = (CQLPrefixNode)cql_node;
			result = new InternalModelNamespaceNode(pn.prefix.name, translate(((CQLPrefixNode)cql_node).subtree));
		}

		return result;
	}



	
	protected void processCQLTermNode(AttrPlusTermNode aptn, CQLTermNode  cql_term_node) {

		aptn.setTerm(cql_term_node.getTerm());
		
		String qualifier = cql_term_node.getIndex();

		if ( ( qualifier != null ) && ( qualifier.length() > 0 ) ) {
			log.debug("Using supplied qualifier : "+qualifier);
			aptn.setAttr(AttrPlusTermNode.ACCESS_POINT_ATTR,process(qualifier));
		}
		else {
			log.debug("Using default qualifier");
			aptn.setAttr(AttrPlusTermNode.ACCESS_POINT_ATTR,process(default_qualifier));
		}

		// CQL Relation object:
		CQLRelation relation = cql_term_node.getRelation();

		if ( relation != null ) {
			if ( relation.getBase() != null ) {
				if ( relation.getBase().equalsIgnoreCase("scr") ) {
					aptn.setAttr(AttrPlusTermNode.RELATION_ATTR,new AttrValue("="));
				}
				else if ( relation.getBase().equalsIgnoreCase("exact") ) {
					aptn.setAttr(AttrPlusTermNode.RELATION_ATTR,new AttrValue("="));
				}
				else if ( relation.getBase().equalsIgnoreCase("all") ) {
					aptn.setAttr(AttrPlusTermNode.RELATION_ATTR,new AttrValue("="));
				}
				else if ( relation.getBase().equalsIgnoreCase("any") ) {
					aptn.setAttr(AttrPlusTermNode.RELATION_ATTR,new AttrValue("="));
				}
				else {
					aptn.setAttr(AttrPlusTermNode.RELATION_ATTR,new AttrValue(relation.getBase()));
				}
			}
		}


	}
	
	
	protected void fixDefaultNamespaces(AttrPlusTermNode aptn, CQLTermNode  cql_term_node) {

		log.debug("Processing attrplustermnode:"+aptn);
		// Look up conversion information for source node

		// set default relation context set
		if ( aptn.getRelation() != null ) {
			AttrValue relation = (AttrValue)aptn.getRelation();

			if ( relation !=null && (relation.getNamespaceIdentifier() == null || force_def  )) {
				relation.setNamespaceIdentifier(this.defRelNamespace);
				log.debug("Processing relation :"+relation);
			}
		}

		// set default attribute context set

		Object ap_node = aptn.getAccessPoint();
		if ( ap_node != null ) {
			AttrValue qualifier = (AttrValue)ap_node;

			if ( qualifier != null && ( qualifier.getNamespaceIdentifier() == null || force_def )) {
				qualifier.setNamespaceIdentifier(this.defAttrNamespace);
				log.debug("Processing AccessPoint :"+qualifier);					

			}

			// FIX incorrect behavior of very old CQL (0.0.7) library
			if (  qualifier != null && 
					qualifier.getNamespaceIdentifier().equalsIgnoreCase("srw") &&
					qualifier.getValue().equalsIgnoreCase("serverChoice")) {
				log.debug("Setting srw context set to cql for serverChoice");
				qualifier.setNamespaceIdentifier("cql");
			}


		}

		// set default structure context set
		if ( aptn.getStructure() != null ) {
			AttrValue structure = (AttrValue)aptn.getStructure();

			if ( structure !=null && (structure.getNamespaceIdentifier() == null || force_def  )) {
				structure.setNamespaceIdentifier(this.defStructNamespace);
				log.debug("Processing structure :"+structure);
			}
		}
	}
	
	private AttrValue process(String s) {
		AttrValue result = null;
		if ( ( s != null ) && ( s.length() > 0 ) ) {
			String[] components = s.split("\\.");
			if ( components.length == 1 ) {
				result=new AttrValue(components[0]);
			}
			else if ( components.length == 2 ) { 
				result=new AttrValue(components[0],components[1]);
			}
			else {
				result = new AttrValue(s);
			}
		}
		return result;
	}

	public String toString() {
		if ( cql_root != null )
			return cql_root.toCQL();

		return null;
	}

	public void setDefAttrNamespace(String defAttrNamespace) {
		this.defAttrNamespace = defAttrNamespace;
	}

	public void setDefRelNamespace(String defRelNamespace) {
		this.defRelNamespace = defRelNamespace;
	}

	public void setDefStructNamespace(String defStructNamespace) {
		this.defStructNamespace = defStructNamespace;
	}
	






}
