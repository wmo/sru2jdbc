/******************************************************************************
 * Copyright (C) 2010 World Meteorological Organization
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as published by
 * the Free Software Foundation; 
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 * 
 * Contact: Timo Pröscholdt <tproescholdt_at_wmo.int> , World Meteorological Organization
 *
 *******************************************************************************/
package org.wmo.jzkitsru2jdbc.utils.config;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.style.ToStringCreator;
import org.springframework.util.Assert;

/**
 * Maps a cql relation to an sql relation. 
 *
 */
public class RelationDBO {
	/** name of the relation. */
	private String name;
	
    /** optional function for this relation. */
    private String function;
    
    /** optional custom processor for this relation. */
    private String custom;
	
	/** tables relevant for this relation. */
	private HashSet<String> tables = new LinkedHashSet<String>();
	
    /** list of sql statements for this relation - ordering is crucial here. */
	private HashSet<String> sqls = new LinkedHashSet<String>();

	/** default mapping values and settings. */
	private DefaultDBO defaultMappings;
	
	/** sets the attribute this relation belongs to. */
	private AttributeDBO parentAttribute;
	
	/** logger object */
    private Log log = LogFactory.getLog(XMLmapper.class);
	
	/** default constructor. */
	public RelationDBO() {
	    super();
	}
	
	/** adds a table to this relation. */
	public void addTable(String table) {
        log.debug("adding table " + table + " to relation " + name);
		tables.add(table);
	}
    
    public Set<String> getTables() {
        return tables;
    }
	
    /** adds a sql fragment to this relation. */
	public void addSql(String sql) {
        log.debug("adding sql " + sql + " to relation " + name);
		sqls.add(sql);
	}
    
    public Set<String> getSqls() {
        return sqls;
    }
    
    /**
     * @return name of this relation
     */
    String getName() {
        return name;
    }
    
    /** sets the name of this relation */
    public void setName(String name) {
        this.name=name;
    }

    /**
     * @return function for this relation
     */
	public String getFunction() {
        return function;
    }

	/**
	 * sets the function for this relation.
	 * @param function the function to set
	 */
    public void setFunction(String function) {
        this.function = function;
    }

    /**
     * @return custom processor for this relation.
     */
    public String getCustom() {
        return custom;
    }

    /**
     * sets the custom processor for this relation.
     * @param custom the custom processor to set
     */
    public void setCustom(String custom) {
        this.custom = custom;
    }

    /**
     * @return default mappings and settings
     */
    public DefaultDBO getDefaultMappings() {
        return defaultMappings;
    }

    /**
     * set the default mappings and settings.
     * @param defaultMappings defaults to set
     */
    public void setDefaultMappings(DefaultDBO defaultMappings) {
        this.defaultMappings = defaultMappings;
    }

    /**
     * @return the attribute this relation belongs to
     */
    public AttributeDBO getParentAttribute() {
        return parentAttribute;
    }

    /**
     * sets the attribute this relation belongs to.
     * @param parentAttribute the attribute
     */
    public void setParentAttribute(AttributeDBO parentAttribute) {
        this.parentAttribute = parentAttribute;
    }

    /**
	 * builds the sql for this relation.
	 * @param aParameter the query parameter(s)
	 * @return sql fragment
	 */
	public SqlQueryFragment getSQLstatement(String aParameter) {
		
		if ( custom == null) {
    		return getPlainSQL(aParameter);
		}
		else {
		    return getCustomSQL(aParameter);
		}
	}
	
	/**
	 * returns a statically generated sql fragment for this relation 
     * @param aParameter the query parameter(s)
	 * @return the generated query fragment
	 */
	private SqlQueryFragment getPlainSQL(String aParameter) {
        String ret = "";
        for (String s : sqls) {
            ret+=s+" ";
        }
        ret=ret.substring(0,ret.length()-1);
	    return new SqlQueryFragment(ret, aParameter, parentAttribute.getColumnType(), parentAttribute.isEscapeParams());
	}
	
    /**
     * returns a dynamically generated sql fragment for this relation using a custom processor 
     * @param aParameter the query parameter(s)
     * @return the generated sql
     */
    private SqlQueryFragment getCustomSQL(String aParameter) {
        CustomMappingProcessor p = (CustomMappingProcessor)defaultMappings.getCtx().getBean(custom);
        Assert.notNull(p, "custom mapping processor '" + custom + "' not found in spring config.");
        return p.map(this, aParameter, parentAttribute.getColumnType(), parentAttribute.isEscapeParams());
    }


    //-------------------------------------------------------------------------
    // misc
    //-------------------------------------------------------------------------
    public String toString() {
        return new ToStringCreator(this)
            .append("name", name)
            .append("function", function)
            .append("tables", tables)
            .append("sqls", sqls)
            .toString();        
    }
    
}
