package org.wmo.jzkitsru2jdbc.utils.config;

/**
 * processor to map bounds index with partial operator to sql.
 * 
 */
public class PartialBoundsProcessor extends BoundsProcessor {

    /**
     * empty default constructor.
     */
    public PartialBoundsProcessor() {
    }

    /**
     * maps the bounds index to a sql statment. this mapping requires the target rectangle to be partially within the
     * specified range as indicated via the argument string.
     * 
     * @param aRelation
     *            the relation
     * @param aValue
     *            the input query value in n/w/s/e form: '56.66 -1.58 45.83 20.21'
     * @param aColumnType
     *            the attributes datatype @see {@link AttributeDBO}
     * @param isEscapeParams true if the special chars inside a param need to be escaped
     *  
     * @return a custom sql string
     */

    @Override
    public SqlQueryFragment map(RelationDBO aRelation, String aValue, String aColumnType, boolean isEscapeParams) {

        final float[] theFloatCoords = parseCoords(aValue);
        final String theSql =
              " ( " + SqlQueryFragment.PLACEHOLDER + " >= " + getColumnNameSouth() + " and " + " "
                    + SqlQueryFragment.PLACEHOLDER + " <= " + getColumnNameNorth() + " and " + " "
                    + SqlQueryFragment.PLACEHOLDER + " >= " + getColumnNameWest() + " and " + " "
                    + SqlQueryFragment.PLACEHOLDER + " <= " + getColumnNameEast() + " ) ";

        final String[][] theParameters = new String[][] { 
             { "" + theFloatCoords[0], AttributeDBO.COLUMN_TYPE_FLOAT },
             { "" + theFloatCoords[2], AttributeDBO.COLUMN_TYPE_FLOAT },
             { "" + theFloatCoords[3], AttributeDBO.COLUMN_TYPE_FLOAT },
             { "" + theFloatCoords[1], AttributeDBO.COLUMN_TYPE_FLOAT },
         };

        final SqlQueryFragment theFragment = new SqlQueryFragment(theSql, theParameters, isEscapeParams);
        return theFragment;
    }
}
