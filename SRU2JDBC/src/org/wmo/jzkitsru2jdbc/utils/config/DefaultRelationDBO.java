package org.wmo.jzkitsru2jdbc.utils.config;

import org.springframework.core.style.ToStringCreator;

/**
 * defines the mapping of a relation operator from cql to sql.
 *
 */
public class DefaultRelationDBO {
    
    /** cql relation operator. */
    private String cql;
       
    /** sql relation operator. */
    private String sql;
    
    /** parameter placeholder. */
    private String placeHolder;

    /** default constructor. */
    public DefaultRelationDBO() {
        super();
    }

    //-------------------------------------------------------------------------
    // getter / setter
    //-------------------------------------------------------------------------
    public String getCql() {
        return cql;
    }

    public void setCql(String cql) {
        this.cql = cql;
    }

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }
    
    public String getPlaceHolder() {
        return placeHolder;
    }

    public void setPlaceHolder(String placeHolder) {
        this.placeHolder = placeHolder;
    }

    //-------------------------------------------------------------------------
    // misc
    //-------------------------------------------------------------------------
    public String toString() {
        return new ToStringCreator(this)
            .append("cql", cql)
            .append("sql", sql)
            .append("placeHolder", placeHolder)
            .toString();        
    }

}
