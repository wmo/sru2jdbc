package org.wmo.jzkitsru2jdbc.utils.config;

import java.util.Hashtable;

import org.springframework.context.ApplicationContext;
import org.springframework.core.style.ToStringCreator;

/**
 * manages default values for cql->sql mappings.
 *
 */
public class DefaultDBO {

    /** default table for mappings. */
    private String defaultTable;
       
    /** resultcolumn containing the metadata document. */
    private String resultColumn;
    
    /** spring context that will be applied after the object has been created from config. */
    private ApplicationContext ctx;
    
    /** default cql->sql relation operators. cql is used as key. */
    private Hashtable<String,DefaultRelationDBO> defaultRelations = new Hashtable<String, DefaultRelationDBO>();
    
    /** default constructor. */
    public DefaultDBO() {
        super();
    }
    
    /** adds a default relation operator mapping to the list of default relation operators.
     * @param rel the DefaultRelation 
     */
    public void registerDefaultRelation(DefaultRelationDBO rel) {
        defaultRelations.put(rel.getCql(),rel);
    }
    
    /**
     * finds the default relation for the given relation name.
     * @param name the name of the relation
     * @return the DefaultRelation found or null
     */
    public DefaultRelationDBO getDefaultRelationFor(String name) {
        return defaultRelations.get(name);
    }

    //-------------------------------------------------------------------------
    // getter / setter
    //-------------------------------------------------------------------------
    public String getDefaultTable() {
        return defaultTable;
    }

    public void setDefaultTable(String defaultTable) {
        this.defaultTable = defaultTable;
    }

    public String getResultColumn() {
        return resultColumn;
    }

    public void setResultColumn(String aResultColumn) {
        this.resultColumn = aResultColumn;
    }
    
    public ApplicationContext getCtx() {
        return ctx;
    }

    public void setCtx(ApplicationContext ctx) {
        this.ctx = ctx;
    }

    //-------------------------------------------------------------------------
    // misc
    //-------------------------------------------------------------------------
    public String toString() {
        return new ToStringCreator(this)
            .append("defaultTable", defaultTable)
            .append("defaultColumn", resultColumn)
            .append("defaultRelations", defaultRelations)
            .toString();        
    }

}
