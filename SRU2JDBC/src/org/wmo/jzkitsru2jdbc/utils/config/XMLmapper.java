/******************************************************************************
 * Copyright (C) 2010 World Meteorological Organization
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as published by
 * the Free Software Foundation; 
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 * 
 * Contact: Timo Pröscholdt <tproescholdt_at_wmo.int> , World Meteorological Organization
 *
 *******************************************************************************/
package org.wmo.jzkitsru2jdbc.utils.config;
import java.io.IOException;
import java.util.Collection;
import java.util.Hashtable;

import org.apache.commons.digester.Digester;
import org.apache.commons.digester.xmlrules.DigesterLoader;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jzkit.search.util.QueryModel.Internal.AttrPlusTermNode;
import org.jzkit.search.util.QueryModel.Internal.ComplexNode;
import org.jzkit.search.util.QueryModel.Internal.InternalModelRootNode;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.io.Resource;
import org.wmo.jzkitsru2jdbc.utils.IndexNotMappedException;
import org.wmo.jzkitsru2jdbc.utils.Query2SQLMapper;
import org.wmo.jzkitsru2jdbc.utils.UnsupportedRelationException;
import org.wmo.jzkitsru2jdbc.utils.Nodes.AttributeQueryNode;
import org.wmo.jzkitsru2jdbc.utils.Nodes.BaseNode;
import org.wmo.jzkitsru2jdbc.utils.Nodes.BooleanNode;
import org.wmo.jzkitsru2jdbc.utils.Nodes.SelectNode;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * main cql->sql mapper class. 
 *
 */
public class XMLmapper implements Query2SQLMapper,ApplicationContextAware,SRU2JDBCConfig {

    /** digester to parse the xml and build mapping objects. */
	private Digester main_digester ;
    /** default values for our mappings. */
    private DefaultDBO defaultMappings = null;
    
	/** list of mappings. */
	private Hashtable<String,AttributeDBO> attributes = new Hashtable<String, AttributeDBO>();
	
	private Log log = LogFactory.getLog(XMLmapper.class);
	private ApplicationContext ctx;

	private String rules;
	private String config;

	/**
	 * create a mapper from parameters (mainly for testing purposes).
	 * @param config config file with cql->sql mappings
	 * @param rules rules file for building helper objects
	 * 
	 * @throws SAXException on configuration problems
	 * @throws IOException  on configuration problems
	 */
	public XMLmapper(InputSource config, InputSource rules)  throws SAXException,IOException  {
	    main_digester = DigesterLoader.createDigester(rules);
	    main_digester.push(this);
	    main_digester.parse(config);
        checkDigesterResult(null);
	}
	
    /**
     * create a mapper from parameters
     * @param config path to config file with cql->sql mappings
     * @param rules path to rules file for building helper objects
     */
	public XMLmapper(String config, String rules) {
		this.rules=rules;
		this.config=config;
	}
	
	/**
	 * spring lifecycle method (called on loading the bean)
	 *  
     * @throws SAXException on configuration problems
     * @throws IOException  on configuration problems
	 */
	public void init() throws SAXException,IOException  {
		
		Resource rules_r = ctx.getResource(rules);
		Resource config_r = ctx.getResource(config);
		
		if ( rules_r != null && config_r != null ) {
		
			log.debug("parsing config:"+rules_r+" rules: "+config_r);
			// create a digester
			main_digester  = DigesterLoader.createDigester( rules_r.getURL() );
            // put the mapper on the main digester stack
			main_digester.push(this);
			// parse the config
			main_digester.parse( config_r.getURL().openStream());
			// check if we can run with the parsed config
			checkDigesterResult(config_r.getFilename());
		}
		else {
			String msg = "Configuration files not found:";
			log.error(msg);
			throw new SAXException(msg);
		}
		
	}
	
	/**
	 * checks if the digester created the necessary objects from config
	 * @param configName name of the config file
	 * @throws SAXException in case of incomplete mappings
	 */
	private void checkDigesterResult(String configName) throws SAXException {
        
        if ( (defaultMappings == null) || (defaultMappings.getResultColumn() == null) ) {
            String msg = "incomplete Defaults in file " + configName + " '" + defaultMappings + "'";
            log.error(msg);
            throw new SAXException(msg);
        }
        
        if ( (attributes == null) || (attributes.size() == 0) ) {
            String msg = "no Attribute in file " + configName;
            log.error(msg);
            throw new SAXException(msg);
        }
	    
	}
	
	
	private String getOpString(int op) 
	{
		switch (op)
		{
		case ComplexNode.COMPLEX_AND:    return "AND";
		case ComplexNode.COMPLEX_ANDNOT: return "NOT";
		case ComplexNode.COMPLEX_OR:     return "OR";
		case ComplexNode.COMPLEX_PROX:   
		default:                         
			throw new RuntimeException("op "+op+" not yet supported");
		}
	}

	public void registerAttribute(AttributeDBO a) {
	    a.applyDefaults(defaultMappings);
		log.debug("registering attribute: "+a);
		attributes.put(a.getName(), a);
	}
	
	public void registerDefaults(DefaultDBO aDefault) {
        log.debug("registering default values: "+aDefault);
        defaultMappings = aDefault;
        defaultMappings.setCtx(ctx);
	}
	
	
	@Override
	public AttributeQueryNode map(AttrPlusTermNode aptn)  {
		
		String theAttrib = getUnqualifiedAttrib(aptn.getAccessPoint().toString());
		String theRelation = getUnqualifiedAttrib(aptn.getRelation().toString());

		if (!attributes.containsKey(theAttrib)) {
			throw new IndexNotMappedException(theAttrib);
		}
		
		AttributeDBO a = attributes.get(theAttrib);
		
		if (!a.relations.containsKey(theRelation)) {
			throw new UnsupportedRelationException(theRelation);
		}
		
		RelationDBO r = a.relations.get(theRelation);

		SqlQueryFragment theQueryFragment = r.getSQLstatement(aptn.getTermAsString(false));
		
		
		AttributeQueryNode node = new AttributeQueryNode(theQueryFragment);
		for ( String s : r.getTables() ) {
			node.addTable(s);
		}
		
		return node;
	}
	
	@Override
	public BaseNode map(ComplexNode cnode)  {
		return new BooleanNode(getOpString(cnode.getOp()));
	}

	@Override
	public SelectNode map(InternalModelRootNode root)  {
		return new SelectNode(defaultMappings.getResultColumn());
	}

	@Override
	public void setApplicationContext(ApplicationContext ctx )
			throws BeansException {
			this.ctx=ctx;
	}
	
	@Override
	public Collection<String> getAttributes() {
		return this.attributes.keySet();
	}
	
	private String getUnqualifiedAttrib(String s) {

		String ret="";
		
		String[] temp = s.split(":");
		if (temp.length==1) {
			ret = temp[0];
		}
		else {
			ret = temp[1];
		}
		return ret;
	}
	
	public String toString() {
		String ret = "";
		
		for (AttributeDBO a : attributes.values() ) {
			ret+=a.toString()+",";
		}
		
		return ret;
	}

//	public static final void main(String[] args) throws SAXException, IOException {
//	    BasicConfigurator.configure();
//	    
//	    InputSource rules = new InputSource(new FileReader("c:\\projects\\gisc-wmo-wis\\sru\\SRU2JDBC\\WebContent\\WEB-INF\\classes\\config\\digesterrules.xml"));
//        InputSource config = new InputSource(new FileReader("c:\\projects\\gisc-wmo-wis\\sru\\SRU2JDBC\\WebContent\\WEB-INF\\classes\\config\\SRU2JDBCmappings.xml"));
//	    XMLmapper m = new XMLmapper(config, rules);
//	}
	
}
