/******************************************************************************
 * Copyright (C) 2010 World Meteorological Organization
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as published by
 * the Free Software Foundation; 
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 * 
 * Contact: Timo Pröscholdt <tproescholdt_at_wmo.int> , World Meteorological Organization
 *
 *******************************************************************************/
package org.wmo.jzkitsru2jdbc.utils.config;

import java.util.Enumeration;
import java.util.Hashtable;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.style.ToStringCreator;

/**
 * Maps an attribute from cql to sql. 
 *
 */
public class AttributeDBO {
    
    /** constant for column types. */
    public static final String COLUMN_TYPE_STRING = "string";
    
    /** constant for column types. */
    public static final String COLUMN_TYPE_INT = "integer";
    
    /** constant for column types. */
    public static final String COLUMN_TYPE_FLOAT = "float";
    
    /** constant for column types. */
    public static final String COLUMN_TYPE_DOUBLE = "double";
    
    /** constant for column types. */
    public static final String COLUMN_TYPE_DATE = "date";
    
    /** constant for column types. */
    public static final String COLUMN_TYPE_TIMESTAMP = "timestamp";
    
	/** name of this attribute. */
	private String name;
    /** column name of this attribute. */
    private String columnName;
    /** column type of this attribute necessary to set parameter values. */
    private String columnType = COLUMN_TYPE_STRING;
	/** 
	 * set to true if special characters have to be escaped. 
	 * currently necessary for oracle text queries. 
	 */
    private boolean escapeParams = false;    
    
    /** list of defined relations for this attribute. */
	Hashtable<String,RelationDBO> relations = new Hashtable<String, RelationDBO>();

	/** logger object */
	private Log log = LogFactory.getLog(XMLmapper.class);
	
	/** default constructor. */
	public AttributeDBO() {
	    super();
        log.debug("creating " + this);
	}
	
	/** adds a relation to this attributes mappings. */
	public void addRelation(RelationDBO rel) {
	    rel.setParentAttribute(this);
		relations.put(rel.getName(),rel);
	}

	/**
	 * applies default values to incompletely configured mappings. 
	 * @param defaultMappings the defaults to fill in
	 */
    public void applyDefaults(DefaultDBO defaultMappings) {
        log.debug("applying defaults for attr " + name + " on " + this);

        // if no columnName is configured columnname defaults to indexname
        if (columnName == null) {
            columnName = name;
        }
        
        Enumeration<RelationDBO> theRelations = relations.elements();
        while (theRelations.hasMoreElements()) {
            RelationDBO theRelation = theRelations.nextElement();
            theRelation.setDefaultMappings(defaultMappings);
            
            // add default table if none is configured
            if (theRelation.getTables().size() == 0) {
                theRelation.addTable(defaultMappings.getDefaultTable());
            }
            
            // add default relations if none are configured
            if (theRelation.getSqls().size() == 0) {
                DefaultRelationDBO theDefaultRelation = 
                    defaultMappings.getDefaultRelationFor(theRelation.getName());
                
                String theSql = ""; 
                // if we have a custom processor we will generate sql dynamically
                if (theRelation.getCustom() == null) {
                    if (theDefaultRelation == null ) {
                        log.error("no default mapping defined for '" + theRelation.getName());
                    }
                    else {
                        // generate sql for column name
                        theSql = columnName + " " + 
                                        theDefaultRelation.getSql() + " ";

                        if (theRelation.getFunction() == null) {
                            // just plain sql
                            theSql += theDefaultRelation.getPlaceHolder();
                        }
                        else {
                            // using a function
                            theSql += theRelation.getFunction();
                        }
                    }
                    theRelation.addSql(theSql);
                }
            }
        }
        log.debug("defaults applied for attr " + name + " on " + this);
    }

    public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

    public void setColumnName(String name) {
        this.columnName = name;
    }

    public String getColumnName() {
        return columnName;
    }
	
	public String getColumnType() {
        return columnType;
    }

    public void setColumnType(String columnType) {
        this.columnType = columnType;
    }

    public boolean isEscapeParams() {
        return escapeParams;
    }

    public void setEscapeParams(boolean escapeParams) {
        this.escapeParams = escapeParams;
    }

    public String toString() {
        return new ToStringCreator(this)
            .append("name", name)
            .append("columnName", columnName)
            .append("relations", relations)
            .toString();        
	}
	
//    public String toString() {
//        
//        String temp = "";
//        for (String r : relations.keySet() ) {
//            temp+=r+",";
//        }
//        
//        return getName()+"("+temp+")";
//    }
}
