package org.wmo.jzkitsru2jdbc.utils.config;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.style.ToStringCreator;
import org.wmo.jzkitsru2jdbc.utils.MaskedWordTooShortException;

/**
 * 
 * contains a fragment of an sql query plus its parameters.
 */
public class SqlQueryFragment {

    /** logger. */
    private static final Log LOG = LogFactory.getLog(SqlQueryFragment.class);

    /** the query frgament. */
    private String queryFragment;
    
    /** ordered list of parameters for this query fragment. */
    private LinkedList<String>parameters = new LinkedList<String>();

    /** ordered list of parametertypes for this query fragment. */
    private LinkedList<String>parameterTypes = new LinkedList<String>();
        
    /** regex to match placeholders within sql strings.
     *  finds anything inside single quotes with optional % wildcards.
     *  by avoiding greedyness we find multiple placeholders. 
     */
    public static final String REGEX_PLACEHOLDER = "%?##VAL##?%?";
    
    /** character codes that need to be escaped. */
    private static final char[] TOESCAPE = {
       92,  // \ new 12.05. must be the first character to escape
       0, 9, 10, 11, 12, 13,                // not printable 
       33,  // !
       36,  // $
       37,  // %
       38,  // &
       40,  // (
       41,  // )
       42,  // *
       44,  // ,
       45,  // -
       58,  // :
       59,  // ;
       60,  // <
       61,  // =
       62,  // >
       63,  // ?
       91,  // [
       93,  // ]
       95,  // _ new 12.05.
       123, // {
       124, // |
       125, // }
       126  // ~
    };
    
    /** escape character. */
    private static final String ESCAPE = "\\";
    
    /** 
     * hardcoded placeholder.
     */
    public static final String PLACEHOLDER = "##VAL##";
    
    /**
     * constructs a new query fragment with a fragment as configured 
     * and a list of values to set as parameters. 
     * the placeholders within the fragment will be replaced with the 
     * prepared statement marker (?) and the arguments will be escaped 
     * with optional wildcards. (currently only %)
     * 
     * @param aFragment the fragment to set.
     * @param aParameter the parameter to set.
     * @param aColumnType the attributes datatype @see {@link AttributeDBO} 
     * @param isEscapeParams true if the special chars inside a param need to be escaped
     * 
     * @throws MaskedWordTooShortException if the parameters length is too short
     */
    public SqlQueryFragment(String aFragment, String aParameter, String aColumnType, boolean isEscapeParams) {
        LOG.debug(aFragment);
        checkParamLength(aParameter);
        Pattern p = Pattern.compile(REGEX_PLACEHOLDER);
        Matcher m = p.matcher(aFragment);
        int count = 0;
        while(m.find()) {
            count++;
//            log.debug("Match number "+count);
//            log.debug("group(): "+m.group());
//            log.debug("start(): "+m.start());
//            log.debug("end(): "+m.end());
            parameters.add(applyWildCardAndEscape(m.group(), aParameter, isEscapeParams));
            parameterTypes.add(aColumnType);
        }
        this.queryFragment = m.replaceAll("?");
    }
    
    /**
     * builds the fragment just like the constructor with one string 
     * argument with the difference that multiple parameter values are
     * supplied. it is the callers responsibility that the number of 
     * placeholders and the number of parameters match.   
     * 
     * @param aFragment the fragment to set.
     * @param aParameters the parameters to set with the corresponding datatype.
     * @param isEscapeParams true if the special chars inside a param need to be escaped
     * 
     * @throws MaskedWordTooShortException if the parameters length is too short
     */
    public SqlQueryFragment(String aFragment, String[][] aParameters, boolean isEscapeParams) {
        Pattern p = Pattern.compile(REGEX_PLACEHOLDER);
        Matcher m = p.matcher(aFragment);
        int count = 0;
        while(m.find()) {
            String[] theParamPlusType = aParameters[count];
            // do not check parameter length because currently this constructor is only called
            // for bounds processing. TODO add a flag whether to check or not
            //checkParamLength(theParamPlusType[0]);
            parameters.add(applyWildCardAndEscape(m.group(), theParamPlusType[0], isEscapeParams));
            parameterTypes.add(theParamPlusType[1]);
            count++;
        }
        this.queryFragment = m.replaceAll("?");
    }

    /**
     * applies optional % wildcards from aPattern to aParameter
     * @param aPattern as configured in mapping config
     * @param aParameter the parameter value
     * @param isEscapeParams true if the special chars inside a param need to be escaped
     * 
     * @return optionally wildcarded parameter value
     */
    private String applyWildCardAndEscape(String aPattern, String aParameter, boolean isEscapeParams) {
        StringBuilder theParam = new StringBuilder();
        if (aPattern.startsWith("%")) {
            theParam.append("%");
        }
        if (isEscapeParams) {
            aParameter = escapeParam(aParameter);
        }
        theParam.append(aParameter);
        if (aPattern.endsWith("%")) {
            theParam.append("%");
        }
        return theParam.toString();
    }
    
    /**
     * escapes special characters inside a parameters value.
     * 
     * @param aParameter unescaped parameter
     * 
     * @return escaped parameter
     */
    private String escapeParam(String aParameter) {
        for ( int i=0; i< TOESCAPE.length; i++ ) {
            aParameter = aParameter.replace(""+TOESCAPE[i], ESCAPE + TOESCAPE[i]);
        }
        return aParameter;
    }

    /**
     * checks the length of a parameter to contain at least 3 non-wildcard characters.
     * 
     * @param aParameter to check
     */
    private void checkParamLength(String aParameter) {
        final char[] theParam = aParameter.toCharArray();
        int countSpecial = 0;
        for (int i = 0; i < theParam.length; i++) {
            for (int j = 0; j < TOESCAPE.length; j++) {
                if (theParam[i] == TOESCAPE[j]) {
                    countSpecial++;
                    break;
                }
            }
        }
        if ( theParam.length - countSpecial < 3 ) {
            throw new MaskedWordTooShortException("Parameter '" + aParameter + "'");
        }
    }
    
    
    
    public void addParameter(String aParameter) {
        parameters.add(aParameter);
    }
    
    public String getQueryFragment() {
        return queryFragment;
    }

    public void setQueryFragment(String queryFragment) {
        this.queryFragment = queryFragment;
    }

    public List<String> getParameters() {
        return parameters;
    }
    
    public LinkedList<String> getParameterTypes() {
        return parameterTypes;
    }

    @Override
    public String toString() {
        return new ToStringCreator(this)
        .append("queryFragment", queryFragment)
        .append("parameters", parameters)
        .append("parameterTypes", parameterTypes)
        .toString();        
    }
    
//    public static final void main(String[] args) {
//
//        String f1 = "title like ##VAL## and blub";
//        String f2 = "title like %##VAL##  or blub";
//        String f3 = "title like ##VAL##% and  not blub";
//        String f4 = "title like %##VAL##% and not ##VAL##% or %##VAL##";
//        String f5 = "north > ##VAL## and south < ##VAL##% and east = %##VAL## and west = %##VAL##";
//        
//        
//        final String[][] theParameters = new String[][] {
//          { "" + 0.57, AttributeDBO.COLUMN_TYPE_FLOAT },
//          { "" + -1.8, AttributeDBO.COLUMN_TYPE_FLOAT },
//          { "" + 180.0, AttributeDBO.COLUMN_TYPE_FLOAT },
//          { "" + -180.0, AttributeDBO.COLUMN_TYPE_FLOAT },
//        };
//        
//        SqlQueryFragment sql1 = new SqlQueryFragment(f4, "h{al*lo", AttributeDBO.COLUMN_TYPE_STRING, true);
//        System.out.println(sql1);
//        
//        SqlQueryFragment sql2 = new SqlQueryFragment(f5, theParameters, false);
//        System.out.println(sql2);
//    }
}
