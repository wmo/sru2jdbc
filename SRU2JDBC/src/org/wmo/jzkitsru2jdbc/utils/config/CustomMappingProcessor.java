package org.wmo.jzkitsru2jdbc.utils.config;

/**
 * interface to a custom mapping processor.
 *
 */
public interface CustomMappingProcessor {

    /**
     * maps the aRelation of aAttr to a custom sql statement 
     * @param aRelation the relation
     * @param aParameter the parameter value 
     * @param aColumnType the attributes datatype @see {@link AttributeDBO} 
     * @param isEscapeParams true if the special chars inside a param need to be escaped
     *  
     * @return a custom sql query fragment
     */
    SqlQueryFragment map(RelationDBO aRelation, String aParameter, String aColumnType, boolean isEscapeParams);
}
