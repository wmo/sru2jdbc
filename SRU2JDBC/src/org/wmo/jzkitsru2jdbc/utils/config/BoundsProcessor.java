package org.wmo.jzkitsru2jdbc.utils.config;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * processor to map bounds index with within and nwse operators to sql.
 *
 */
public class BoundsProcessor implements CustomMappingProcessor {

    /** logger object */
    private Log log = LogFactory.getLog(BoundsProcessor.class);
    
    /** name of the north column in the database. */
    private String columnNameNorth;
    /** name of the west column in the database. */
    private String columnNameWest;
    /** name of the south column in the database. */
    private String columnNameSouth;
    /** name of the east column in the database. */
    private String columnNameEast;

    /**
     * empty default constructor.
     */
    public BoundsProcessor() {
    }
    
    /**
     * maps the bounds index to a sql statment. this mapping requires the target
     * rectangle to be completely within the specified range as indicated via 
     * the argument string.
     *  
     * @param aRelation the relation
     * @param aValue the input query value in n/w/s/e form: '56.66 -1.58 45.83 20.21'
     * @param aColumnType the attributes datatype @see {@link AttributeDBO} 
     * @param isEscapeParams true if the special chars inside a param need to be escaped
     *  
     * @return a custom sql string
     */

    @Override
    public SqlQueryFragment map(RelationDBO aRelation, String aValue, String aColumnType, boolean isEscapeParams) {
        
        final float[] theFloatCoords = parseCoords(aValue);
        
        final String theSql =   
            " ( " + getColumnNameNorth() + " <= " + SqlQueryFragment.PLACEHOLDER + " and " +
            " " + getColumnNameSouth() + " >= " + SqlQueryFragment.PLACEHOLDER + " and " +
            " " + getColumnNameWest() + " >= " + SqlQueryFragment.PLACEHOLDER + " and " +
            " " + getColumnNameEast() + " <= " + SqlQueryFragment.PLACEHOLDER + " ) ";
        
        final String[][] theParameters = new String[][] {
          { "" + theFloatCoords[0], AttributeDBO.COLUMN_TYPE_FLOAT },
          { "" + theFloatCoords[2], AttributeDBO.COLUMN_TYPE_FLOAT },
          { "" + theFloatCoords[1], AttributeDBO.COLUMN_TYPE_FLOAT },
          { "" + theFloatCoords[3], AttributeDBO.COLUMN_TYPE_FLOAT },
        };
        
        final SqlQueryFragment theFragment = new SqlQueryFragment(theSql, theParameters, isEscapeParams);
        return theFragment;
    }

    /**
     * convert input coordinates to a float array 
     * @param aValue input value
     * @return array of coordinates as n/w/s/e
     */
    protected float[] parseCoords(String aValue) {
        
        float[] theFloatCoords = new float[4];
        String[] theStringCoords = aValue.split("[ ]");
        if (theStringCoords.length != 4) {
            log.warn("not exactly 4 arguments for bounds mapping '" + aValue + "' ");
        }
        int i=0;
        for (; i< theStringCoords.length; i++) {
            try {
                theFloatCoords[i] = Float.parseFloat(theStringCoords[i]);
            } catch (NumberFormatException aNfe) {
                log.warn("cannot parse coordinate value '" + theStringCoords[i] + "' - using default.");
                theFloatCoords[i] = getDefault(i);
            }
        }
        while (i<4) {
            theFloatCoords[i] = getDefault(i);
            i++;
        }
        return theFloatCoords;
    }
    
    /**
     * return a defualt value for a coordinate
     * @param aIdx index  0=n 1=w 2=s 3=e
     * @return default value
     */
    private float getDefault(int aIdx) {
        switch(aIdx) {
            case 0: return 90.0f;
            case 1: return -90.0f;
            case 2: return -180.0f;
            case 3: return 180.0f;
            default: return Float.NaN;
        }
    }

    public String getColumnNameNorth() {
        setDefaultIfNull(columnNameNorth, "north");
        return columnNameNorth;
    }

    public void setColumnNameNorth(String columnNameNorth) {
        this.columnNameNorth = columnNameNorth;
    }

    public String getColumnNameWest() {
        setDefaultIfNull(columnNameWest, "west");
        return columnNameWest;
    }

    public void setColumnNameWest(String columnNameWest) {
        this.columnNameWest = columnNameWest;
    }

    public String getColumnNameSouth() {
        setDefaultIfNull(columnNameSouth, "south");
        return columnNameSouth;
    }

    public void setColumnNameSouth(String columnNameSouth) {
        this.columnNameSouth = columnNameSouth;
    }

    public String getColumnNameEast() {
        setDefaultIfNull(columnNameEast, "east");
        return columnNameEast;
    }

    public void setColumnNameEast(String columnNameEast) {
        this.columnNameEast = columnNameEast;
    }

    /** sets a default value for a columnname if not configured. */
    private void setDefaultIfNull(String aColumnName, String aDefault) {
        if (aColumnName == null) {
            aColumnName = aDefault;
        }
    }
}
