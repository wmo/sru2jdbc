/******************************************************************************
 * Copyright (C) 2010 World Meteorological Organization
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as published by
 * the Free Software Foundation; 
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 * 
 * Contact: Timo Pröscholdt <tproescholdt_at_wmo.int> , World Meteorological Organization
 *
 *******************************************************************************/
package org.wmo.jzkitsru2jdbc.utils;

import java.util.Collection;

import org.jzkit.search.util.QueryConversion.QueryConverterException;
import org.jzkit.search.util.QueryModel.Internal.AttrPlusTermNode;
import org.jzkit.search.util.QueryModel.Internal.AttrValue;
import org.jzkit.search.util.QueryModel.Internal.ComplexNode;
import org.jzkit.search.util.QueryModel.Internal.InternalModelRootNode;
import org.jzkit.search.util.QueryModel.Internal.QueryNode;
import org.wmo.jzkitsru2jdbc.utils.Nodes.AttributeQueryNode;
import org.wmo.jzkitsru2jdbc.utils.Nodes.BaseNode;
import org.wmo.jzkitsru2jdbc.utils.Nodes.BooleanNode;
import org.wmo.jzkitsru2jdbc.utils.Nodes.SelectNode;
import org.wmo.jzkitsru2jdbc.utils.config.SRU2JDBCConfig;

public class SRUQuery2SQLMapper /*implements Query2SQLMapper,SRU2JDBCConfig */ {

	//public BaseNode map(QueryNode node) throws QueryConverterException {
	//	return null;
	//}
	
	
	/* (non-Javadoc)
	 * @see org.wmo.jzkitsru2jdbc.utils.Query2SQLMapper#map(org.jzkit.search.util.QueryModel.Internal.AttrPlusTermNode)
	 */
//	public BaseNode map(AttrPlusTermNode aptn)   {
//
//		BaseNode qn = null;
//
//		if (aptn.getAccessPoint() != null  && aptn.getRelation() != null ) {
//
//			String attribute =  getAttrVal((AttrValue)aptn.getAccessPoint()) ;
//			String relation =  getAttrVal((AttrValue)aptn.getRelation()) ;
//			String value = aptn.getTermAsString(false);
//
//			if ( attribute.equalsIgnoreCase("title") ) {
//
//				if (relation.equalsIgnoreCase("=")) {
//					qn = new AttributeQueryNode(" title like '%"+value+"%' ");
//					qn.addTable( "gisc_products"  );
//				}
//			}
//
//
//			if (attribute.equalsIgnoreCase("author")) {
//
//				if (relation.equalsIgnoreCase("=")) {
//					qn = new AttributeQueryNode(" gisc_products.id_owner = gisc_meta_contact.id_contact AND upper(gisct_meta_contact) like '%"+value+"%'  ");
//					qn.addTable( "gisc_products"  );
//					qn.addTable("gisc_meta_contact");
//				}
//
//			}
//
//
//		}
//		return qn;
//	}
//
//	/* (non-Javadoc)
//	 * @see org.wmo.jzkitsru2jdbc.utils.Query2SQLMapper#map(org.jzkit.search.util.QueryModel.Internal.ComplexNode)
//	 */
//	public BaseNode map(ComplexNode cnode)  {
//		return new BooleanNode(getOpString(cnode.getOp()));
//	}
//
//
//	/* (non-Javadoc)
//	 * @see org.wmo.jzkitsru2jdbc.utils.Query2SQLMapper#map(org.jzkit.search.util.QueryModel.Internal.InternalModelRootNode)
//	 */
//	public SelectNode map(InternalModelRootNode root)  {
//
//		return new SelectNode();
//	}
//
//
//	/**
//	 * @param val
//	 * @return
//	 * extracts the last index of an attribute (e.G 1.4 becomes 4)
//	 */
//	private String getAttrVal(AttrValue val) {
//
//		if (val == null || val.getValue() == null ) return null;
//
//		String value = val.getValue();
//		String ret=value;
//
//		String[] temp = value.split("\\.");
//
//		if (temp!=null && temp.length>1 ) {
//			ret = temp[temp.length-1];
//		}
//
//		return ret;
//	}
//
//	private String getOpString(int op) 
//	{
//		switch (op)
//		{
//		case ComplexNode.COMPLEX_AND:    return "AND";
//		case ComplexNode.COMPLEX_ANDNOT: return "NOT";
//		case ComplexNode.COMPLEX_OR:     return "OR";
//		case ComplexNode.COMPLEX_PROX:   
//		default:                         
//			throw new RuntimeException("op "+op+" not yet supported");
//		}
//	}
//
//	@Override
//	public Collection<String> getAttributes() {
//		// TODO Auto-generated method stub
//		return null;
//	}


}
