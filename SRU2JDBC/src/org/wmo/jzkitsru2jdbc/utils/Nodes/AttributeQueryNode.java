/******************************************************************************
 * Copyright (C) 2010 World Meteorological Organization
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as published by
 * the Free Software Foundation; 
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 * 
 * Contact: Timo Pröscholdt <tproescholdt_at_wmo.int> , World Meteorological Organization
 *
 *******************************************************************************/
package org.wmo.jzkitsru2jdbc.utils.Nodes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.wmo.jzkitsru2jdbc.utils.config.SqlQueryFragment;

/**
 * dynamically constructed attribute node containig a fragment of an sql query.
 *
 */
public class AttributeQueryNode extends BaseNode {
	
	private static Log log = LogFactory.getLog(AttributeQueryNode.class);
	
	/** the fragment for sql generation */
	private SqlQueryFragment queryFragment;
	
	/**
	 * construct a AttributeQueryNode
	 * 
	 * @param aQueryFragment query fragment
	 */
	public AttributeQueryNode(SqlQueryFragment aQueryFragment) {
		super(aQueryFragment.getQueryFragment());
		this.queryFragment = aQueryFragment;
	}

    public SqlQueryFragment getQueryFragment() {
        return queryFragment;
    }

    public void setQueryFragment(SqlQueryFragment queryFragment) {
        this.queryFragment = queryFragment;
    }
	
}
