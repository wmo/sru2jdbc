/******************************************************************************
 * Copyright (C) 2010 World Meteorological Organization
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as published by
 * the Free Software Foundation; 
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 * 
 * Contact: Timo Pröscholdt <tproescholdt_at_wmo.int> , World Meteorological Organization
 *
 *******************************************************************************/
package org.wmo.jzkitsru2jdbc.utils.Nodes;

import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class SelectNode extends BaseNode {
	
	private static Log log = LogFactory.getLog(SelectNode.class);
	
	private String almostQuery ;
	private Set<String> tables;
    
    /** the list of parameters for sql generation */
    private List<String> parameters;
    
    /** the list of parameter typess for sql generation */
    private List<String> parameterTypes;

	/**
	 * creates a SelectNode with the result column to be queried
	 * @param aResultColumn the resultcolumn
	 */
	public SelectNode(String aResultColumn) {
		super(aResultColumn);
	}

    
    public void setTables(Set<String> tables) {
        this.tables=tables;
    }
    
    /**
     * builds the sql query using payload as resultcolumn. 
     * @return sql to execute
     */
    public String getQuery() {
        return getQuery(getPayLoad());
    }
	
	/**
	 * builds the sql query. 
	 * @param aColumn the resultcolumn to use
	 * @return sql to execute
	 */
	public String getQuery(String aColumn) {
		
		String stables = "";
		for (String s : tables) {
			stables += s+",";
		}
		stables = stables.substring(0, stables.length()-1);
		
		return "SELECT "+aColumn+" FROM " + stables + " WHERE " + almostQuery; 
		
	}

    public List<String> getParameters() {
        return parameters;
    }

    public void setParameters(List<String> parameters) {
        this.parameters = parameters;
    }

    public List<String> getParameterTypes() {
        return parameterTypes;
    }


    public void setParameterTypes(List<String> parameterTypes) {
        this.parameterTypes = parameterTypes;
    }


    public String getAlmostQuery() {
        return almostQuery;
    }
    
    public void setAlmostQuery(String almostQuery) {
        this.almostQuery = almostQuery;
    }
    
}
