/******************************************************************************
 * Copyright (C) 2010 World Meteorological Organization
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as published by
 * the Free Software Foundation; 
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 * 
 * Contact: Timo Pröscholdt <tproescholdt_at_wmo.int> , World Meteorological Organization
 *
 *******************************************************************************/
package org.wmo.jzkitsru2jdbc.utils;

/**
 * this exception is thrown when we do not support a relation
 * for a given index.
 *
 */
public class UnsupportedRelationException extends RuntimeException {
    /** serial version. */
    private static final long serialVersionUID = 6825541474118937347L;

    /** 
     * construct a new {@link UnsupportedRelationException}
     * @param aRelationName the index thats not mapped
     */
    public UnsupportedRelationException(String aRelationName) {
		super(aRelationName);
	}

}
