/******************************************************************************
 * Copyright (C) 2010 World Meteorological Organization
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as published by
 * the Free Software Foundation; 
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 * 
 * Contact: Timo Pröscholdt <tproescholdt_at_wmo.int> , World Meteorological Organization
 *
 *******************************************************************************/



package org.wmo.jzkitsru2jdbc.provider.JDBC;

import java.util.HashMap;
import java.util.Map;
import java.util.Observer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jzkit.search.provider.iface.IRQuery;
import org.jzkit.search.provider.iface.Searchable;
import org.jzkit.search.util.ResultSet.IRResultSet;
import org.springframework.context.ApplicationContext;
import org.wmo.jzkitsru2jdbc.utils.JDBCQueryDecoder;


/**
 * this class implements a JZKit IR provider and needs to be configured 
 * in the JZKit config file. we are useing the jndi-name as a injectable
 * property here.
 */ 
public class JDBCSearchable implements Searchable
{
	private Map record_archetypes = new HashMap();
	private ApplicationContext ctx;
	private static Log log = LogFactory.getLog(JDBCSearchable.class);
	private String JNDIname ;
	


	public JDBCSearchable() {
		log.debug("JDBCSearchable::constructor: empty");
	}

	public JDBCSearchable(String JNDIname) {
		this.JNDIname=JNDIname;
		log.debug("JDBCSearchable:: JNDIname: "+JNDIname);
	}


	public void close() {
		log.debug("JDBCSearchable::close");
	}

	public IRResultSet evaluate(IRQuery q) {
		return evaluate(q,null,null);
	}

	public IRResultSet evaluate(IRQuery q, Object user_info) {
		return evaluate(q, user_info, null);
	}

	public IRResultSet evaluate(IRQuery q, Object user_info, Observer[] observers) {
		
		log.debug("JDBCSearchable::evaluate: begin");

		JDBCQueryDecoder qd = new JDBCQueryDecoder(q.query , ctx) ; 
		JDBCResultSet result = new JDBCResultSet(qd,JNDIname);

		log.debug("JDBCSearchable::evaluate: end");
		
		return result;
	}
	
	public void setJNDIname(String JNDIname) {
		this.JNDIname=JNDIname;
	}

	public void setRecordArchetypes(Map record_syntax_archetypes) {
		this.record_archetypes = record_syntax_archetypes;
	}

	public Map getRecordArchetypes() {
		return record_archetypes;
	}

	public void setApplicationContext(ApplicationContext ctx) {
		this.ctx = ctx;
	}
}
