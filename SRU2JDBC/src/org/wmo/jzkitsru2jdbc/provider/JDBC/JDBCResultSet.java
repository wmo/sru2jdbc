/******************************************************************************
 * Copyright (C) 2010 World Meteorological Organization
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as published by
 * the Free Software Foundation; 
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 * 
 * Contact: Timo Pröscholdt <tproescholdt_at_wmo.int> , World Meteorological Organization
 *
 *******************************************************************************/

package org.wmo.jzkitsru2jdbc.provider.JDBC;

import java.sql.Clob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jzkit.search.util.RecordModel.ExplicitRecordFormatSpecification;
import org.jzkit.search.util.RecordModel.InformationFragment;
import org.jzkit.search.util.RecordModel.RecordFormatSpecification;
import org.jzkit.search.util.ResultSet.AbstractIRResultSet;
import org.jzkit.search.util.ResultSet.IFSNotificationTarget;
import org.jzkit.search.util.ResultSet.IRResultSet;
import org.jzkit.search.util.ResultSet.IRResultSetException;
import org.jzkit.search.util.ResultSet.IRResultSetInfo;
import org.jzkit.search.util.ResultSet.IRResultSetStatus;
import org.wmo.jzkitsru2jdbc.servlets.diag.SruChecker;
import org.wmo.jzkitsru2jdbc.utils.JDBCQueryDecoder;
import org.wmo.jzkitsru2jdbc.utils.Nodes.SelectNode;
import org.wmo.jzkitsru2jdbc.utils.config.AttributeDBO;

/**
 * this class implements a JZKit IRResultSet. The framework will call the relevant methods when query results need to be
 * delivered.
 * 
 * @author Timo Proescholdt <tproescholdt@wmo.int>
 */
public class JDBCResultSet extends AbstractIRResultSet implements IRResultSet {

    /** oracle text error code for the wildcard expansion bug. it seems to be pretty random
     * which errorcode we get - we test against the following ones. */
    private static final int[] WILDCARD_ERROR_CODES = new int[] {
            29902, // Fehler bei der Ausführung von Routine ODCIIndexStart()
            20000, // Oracle Text error
            51030  // wildcard query expansion resulted in too many terms 
    };
    
    /** logger. */
    private static Log log = LogFactory.getLog(JDBCResultSet.class);

    /** number of hits for this query. */
    private int num_hits = 0;

    /** connection for this query. */
    private Connection conn;

    /** resultset for this query. */
    private ResultSet rs = null;

    /** prepared statement for this query. */
    private PreparedStatement pstmt = null;

    /**
     * creates a sql resultset by preparing and executing a query against the database. the resultset will be cached as
     * an instance member as long as the IR resultset remains open.
     * 
     * @param qd
     *            the querydecoder used
     * @param aJndiName
     *            the jndiname for the database connection as configured within the JZKit config
     */
    public JDBCResultSet(JDBCQueryDecoder qd, String aJndiName) {
        log.debug("constructor jndiname: " + aJndiName);

        try {
            // trigger query decoding
            SelectNode theSelectNode = qd.decodeQuery();

            // create a connection
            createDbConnection(aJndiName);

            // count the number of records for this query
            num_hits = getRowCount(theSelectNode);

            // now get the real results
            rs = prepareAndExecute(theSelectNode.getQuery(), theSelectNode);
            log.debug("resultset successfully created!");

            setStatus(IRResultSetStatus.COMPLETE);

        }
        catch (SQLException e) {
            log.error("error creating Resultset due to DB problems: ", e);
            setStatus(IRResultSetStatus.FAILURE);
            close();
        }
        catch (NamingException ne) {
            log.error("error on JNDI-lookup for " + aJndiName, ne);
            setStatus(IRResultSetStatus.FAILURE);
        }
        catch (NumberFormatException nfe) {
            log.error("exception on setting parameters", nfe);
            setStatus(IRResultSetStatus.FAILURE);
            close();
        }
        catch (IllegalArgumentException ie) {
            log.error("exception on setting parameters", ie);
            setStatus(IRResultSetStatus.FAILURE);
            close();
        }
    }

    /**
     * opens a new connection from the connection pool and stores it as long as the result is open.
     * 
     * @param aJndiName
     *            the jndiname to get the connection from
     * @throws NamingException
     *             if lookup fails
     * @throws SQLException
     *             in case of db errors
     */
    private void createDbConnection(String aJndiName) throws NamingException, SQLException {
        Context initContext = new InitialContext();
        Context envContext = (Context)initContext.lookup("java:/comp/env");
        DataSource ds = (DataSource)envContext.lookup(aJndiName);
        conn = ds.getConnection();
        log.debug("opened db connection on " + aJndiName);
    }

    /**
     * prepares and executes the query with the passed in parameters
     * 
     * @param aQueryString
     *            the querystring
     * @param parameters
     *            the parameters list
     * 
     * @return the resultset
     * 
     * @throws SQLException
     */
    private ResultSet prepareAndExecute(String aQueryString, SelectNode aSelect) throws SQLException {
        List<String> parameters = aSelect.getParameters();
        List<String> parameterTypes = aSelect.getParameterTypes();
        
        log.debug("execute sql: '" + aQueryString + "'");
        log.debug("parameters: " + parameters);

        pstmt = conn.prepareStatement(aQueryString, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);

        // unfortunately not supported by oracle driver
        //ParameterMetaData theParamMeta = pstmt.getParameterMetaData();

        int theCount = parameters.size();
        for ( int i=0; i<theCount; i++) {
            String theParamValue = parameters.get(i);
            String theParamType = parameterTypes.get(i);
            setParamValue(pstmt, i+1, theParamValue, theParamType);
        }
        return pstmt.executeQuery();
    }

    /**
     * sets the parameter value for the statement
     * 
     * @param aStmt
     *            the prepared statement
     * @param aParamIdx
     *            the params index
     * @param aParamValue
     *            the param value
     * @param aParamType
     *            the params type
     */
    private void setParamValue(PreparedStatement aStmt, int aParamIdx, String aParamValue, String aParamType) throws SQLException {
        if (aParamType.equalsIgnoreCase(AttributeDBO.COLUMN_TYPE_STRING)) {
            pstmt.setString(aParamIdx, aParamValue);
        } 
        else if (aParamType.equalsIgnoreCase(AttributeDBO.COLUMN_TYPE_INT)) {
            pstmt.setInt(aParamIdx, Integer.parseInt(aParamValue));
        }
        else if (aParamType.equalsIgnoreCase(AttributeDBO.COLUMN_TYPE_FLOAT)) {
            pstmt.setFloat(aParamIdx, Float.parseFloat(aParamValue));
        }
        else if (aParamType.equalsIgnoreCase(AttributeDBO.COLUMN_TYPE_DOUBLE)) {
            pstmt.setDouble(aParamIdx, Double.parseDouble(aParamValue));
        }
        else if (aParamType.equalsIgnoreCase(AttributeDBO.COLUMN_TYPE_DATE)) {
            pstmt.setDate(aParamIdx, Date.valueOf(aParamValue));
        }
        else if (aParamType.equalsIgnoreCase(AttributeDBO.COLUMN_TYPE_TIMESTAMP)) {
            pstmt.setTimestamp(aParamIdx, Timestamp.valueOf(aParamValue));
        }
        else {
            pstmt.setObject(aParamIdx, aParamValue);
        }
    }

    /**
     * 
     * @param aSelectNode
     *            node containing querystring and parameters
     * 
     * @return number of rows that will be returned by this query
     * 
     * @throws SQLException
     */
    private int getRowCount(SelectNode aSelectNode) throws SQLException {
        int theRet = -1;
        try {
            rs = prepareAndExecute(aSelectNode.getQuery("count(*)"), aSelectNode);
        }
        catch (SQLException theSqlEx) {
            if (isWildcardError(theSqlEx.getErrorCode())) {
                log.debug("rewritten parameters because of SQL-Wildcard bug");
                if (strippedWildcards(aSelectNode)) {
                    rs = prepareAndExecute(aSelectNode.getQuery("count(*)"), aSelectNode);
                }
                else {
                    throw theSqlEx;
                }
            }
            else {
                throw theSqlEx;
            }
        }
        rs.next();
        theRet = rs.getInt(1);
        log.debug("number of rows matching: " + theRet);
        rs.close();
        pstmt.close();
        return theRet;
    }
    
    /**
     * test if the given code is one of the wildcard bugs.
     * 
     * @param aCode sql code to test
     * 
     * @return true if is a wildcard error
     */
    private boolean isWildcardError(int aCode) {
        for (int i=0; i<WILDCARD_ERROR_CODES.length; i++) {
            if (aCode == WILDCARD_ERROR_CODES[i]) {
                return true;
            }
        }
        return false;
    }

    /**
     * examines parameter values if they do contain a character sequence that is known to produce
     * bugs in oracle text. if one is found we alter the parameter value to avoid the bug.
     * 
     * @param aSelectNode sql representation 
     * 
     * @return true if any modification was done.
     */
    private boolean strippedWildcards(SelectNode aSelectNode) {
        boolean isStripped = false;
        List<String> parameters = aSelectNode.getParameters();
        for ( int i=0; i<parameters.size(); i++) {

            String theParamValue = parameters.get(i);
            if (theParamValue.matches(".*[^a-zA-Z0-9]%$")) {
                theParamValue = theParamValue.substring(0, theParamValue.length() - 1);
                isStripped = true;
                parameters.set(i, theParamValue);
            }
            if (theParamValue.matches("%[^a-zA-Z0-9].*")) {
                theParamValue = theParamValue.substring(1, theParamValue.length());
                isStripped = true;
                parameters.set(i, theParamValue);
            }
        }
        return isStripped;
    }

    /**
     * provide a block of rows as an array of InformationFragment.
     * 
     * @param starting_fragment
     *            the first rownumber (0-based)
     * @param number
     *            of rows to deliver
     * 
     * @return an array of fragments
     */
    public InformationFragment[] getFragment(int starting_fragment, int count, RecordFormatSpecification spec)
                                                                                                              throws IRResultSetException {

        log.debug("getFragment from:" + starting_fragment + " count: " + count);

        final List<InformationFragment> resultList = new ArrayList<InformationFragment>();
        try {

            boolean hasNext = this.rs.absolute(starting_fragment);
            for (int i = 0; hasNext && i < count; i++) {
//                 log.debug("getFragment fetching row:" + (starting_fragment + i));

                String theClob = null;
                if (conn.isClosed()) {
                    // getFragment is called asynchronously and the connection might not be available.
                    // we can't simply return null or throew an exception here because jzkit fails 
                    // if we are inconsistent with the previously return fragment count.
                    log.error("can't fetch record " + starting_fragment + i + "DB Connection is closed.");
                    theClob = getErrorValue();
                    hasNext = true; // exit loop on count
                }
                else {
                    theClob = getClob(this.rs.getClob(1));
                    hasNext = this.rs.next();
                }
                //log.debug(theClob);
                resultList.add(new org.jzkit.search.util.RecordModel.InformationFragmentImpl(starting_fragment + i,
                                                                                             "REPO",
                                                                                             "COLL",
                                                                                             null,
                                                                                             theClob,
                                                                                             new ExplicitRecordFormatSpecification("xml:ISO19139:F")));
            }

        }
        catch (SQLException e) {
            log.error("Problem fetching Records from DB: ", e);
            return null;
        }

        if (resultList.size() > 0) {
            log.debug(resultList.size() + " fragments fetched.");
            return resultList.toArray(new InformationFragment[resultList.size()]);
        }
        return null;
    }    
    
//    public InformationFragment[] getFragment(int starting_fragment, int count, RecordFormatSpecification spec)
//                                                                                                              throws IRResultSetException {
//
//        log.debug("getFragment from:" + starting_fragment + " count: " + count);
//
//        if (starting_fragment > num_hits) {
//            // TODO try to return null here..
//            String errormsg =
//                              "starting position greater then result set: start: " + starting_fragment + " num_hits: "
//                                  + num_hits;
//            log.error(errormsg);
//            throw new IRResultSetException(errormsg);
//        }
//
//        // if starting_frag is close to the end of the result set, the result array might be to big (should not contain
//        // null values)
//        int size = count;
//        if ((starting_fragment - 1) + count >= this.num_hits) {
//            size = num_hits - (starting_fragment);
//        }
//
//        log.debug("creating result of size: " + size);
//        InformationFragment[] result = new InformationFragment[size];
//
//        try {
//            // TODO should we position at starting_fragment - 1 ??
//            this.rs.absolute(starting_fragment);
//            boolean hasNext = this.rs.next();
//            for (int i = 0; i < size && hasNext; i++) {
//
//                log.debug("getFragment fetching row:" + (starting_fragment + i));
//
//                result[i] =
//                            new org.jzkit.search.util.RecordModel.InformationFragmentImpl(starting_fragment + i,
//                                                                                          "REPO",
//                                                                                          "COLL",
//                                                                                          null,
//                                                                                          getClob(this.rs.getClob(1)),
//                                                                                          new ExplicitRecordFormatSpecification("xml:ISO19139:F"));
//
//                hasNext = this.rs.next();
//            }
//
//        }
//        catch (SQLException e) {
//            String err = "Problem fetching Records from DB: " + e;
//            e.printStackTrace();
//            log.error(err);
//
//            throw new IRResultSetException(err);
//        }
//
//        log.debug("end");
//
//        return result;
//    }

    public void asyncGetFragment(int starting_fragment, int count, RecordFormatSpecification spec,
                                 IFSNotificationTarget target) {
        try {
            InformationFragment[] result = getFragment(starting_fragment, count, spec);
            target.notifyRecords(result);
        }
        catch (IRResultSetException irrse) {
            target.notifyError("wmogeo", new Integer(0), "Problem obtaining result records", irrse);
        }
    }

    /** Current number of fragments available */
    public int getFragmentCount() {
        return num_hits;
    }

    /** The size of the result set (Estimated or known) */
    public int getRecordAvailableHWM() {
        return num_hits;
    }

    /** Release all resources and shut down the object */
    public void close() {
        if (rs != null) {
            try {
                rs.close();
                log.debug("sql resultset closed");

            }
            catch (Exception e) {
                log.error("error closing sql resultset", e);
            }
        }
        if (pstmt != null) {
            try {
                pstmt.close();
                log.debug("statement closed");

            }
            catch (Exception e) {
                log.error("error closing sql pstmt", e);
            }
        }
        if (conn != null) {
            try {
                conn.close();
                log.debug("db connection closed");

            }
            catch (Exception e) {
                log.error("error closing db connection", e);
            }
        }
    }

    public IRResultSetInfo getResultSetInfo() {
        return new IRResultSetInfo(getResultSetName(), getFragmentCount(), getStatus());
    }

    private String getClob(Clob clob) {

        String ret = getNoValue();

        try {

            if (clob != null && clob.length() > 0) {

                int clobLength = (int)clob.length();
                ret = clob.getSubString(1, clobLength);
            }

        }
        catch (SQLException e) {
        }
        ;

        return ret;

    }
    
    
    
    private String getNoValue() {
        return "<novalue/>";
    }
    
    private String getErrorValue() {
        return "<" + SruChecker.DIAG_GENERAL_ERROR + "/>";    
    }
}
