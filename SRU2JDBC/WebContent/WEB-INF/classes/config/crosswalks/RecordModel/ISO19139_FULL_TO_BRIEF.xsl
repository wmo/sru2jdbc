<?xml version="1.0"?>
 
<xsl:stylesheet version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:gmd="http://www.isotc211.org/2005/gmd" 
	xmlns:gco="http://www.isotc211.org/2005/gco" 
	xmlns:gml="http://www.opengis.net/gml" 
	xmlns:xlink="http://www.w3.org/1999/xlink" 
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
	xsi:schemaLocation="http://www.isotc211.org/2005/gmd http://www.isotc211.org/2005/gmd/gmd.xsd">

<xsl:output method="xml"/>

<xsl:template match="/">
  <testroot><test>
  <xsl:for-each select="//gmd:fileIdentifier/gco:CharacterString">
    <inner>
	<xsl:value-of select="text()"></xsl:value-of>
  	</inner>
  </xsl:for-each>
  </test>
  </testroot>
</xsl:template>


</xsl:stylesheet>
