<%@page contentType="text/xml"%><?xml version="1.0" encoding='UTF-8'?><%@page
	import="org.jzkit.search.util.RecordModel.InformationFragment"%>

<% if (request.getAttribute("stylesheet") != null) { %>
<?xml-stylesheet type="text/xsl" href="<%= request.getAttribute("stylesheet") %>"?>
<% } %>

<sru:searchRetrieveResponse   xmlns:sru="http://www.loc.gov/zing/srw/"
	                            xmlns:zr="http://explain.z3950.org/dtd/2.0/"
	                            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                            	xsi:schemaLocation="http://www.loc.gov/zing/srw/ http://www.loc.gov/standards/sru/xml-files/srw-types.xsd">


	<sru:version><%= request.getAttribute("version") %></sru:version>
	<sru:numberOfRecords><%= request.getAttribute("numberRecords") %></sru:numberOfRecords>

	<% if (request.getAttribute("diagnostics") != null) { %>
	<%@include file="diagnostic.jspf"%>
	<% } else { %>

	<sru:resultSetId><%= request.getAttribute("resultSetId") %></sru:resultSetId>
	<% String[] temp = (String[])request.getAttribute("records2") ; 
   InformationFragment[] records = (InformationFragment[])request.getAttribute("records") ;
   if ( records != null && records.length > 0) { %>
	<sru:records>
		<%   for (int i=0;  i< records.length ; i++) {  %>
		<sru:record>
			<sru:recordSchema>info:srw/schema/1/example-v1.1</sru:recordSchema>
			<sru:recordPacking><%= records[i].getFormatSpecification() %></sru:recordPacking>
			<sru:recordData><%= temp[i] %>
			</sru:recordData>
			<sru:recordPosition><%= new Integer(i+ (Integer)request.getAttribute("firstRecord") ) %></sru:recordPosition>
		</sru:record>
		<%   } %>
	</sru:records>
	<% } %>
	<%@include file="searchRetrieveFooter.jspf"%>
	<% } %>

</sru:searchRetrieveResponse>