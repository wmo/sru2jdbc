<%@page contentType="text/xml"%><?xml version="1.0" encoding='UTF-8'?><%@page import="org.wmo.jzkitsru2jdbc.servlets.WMOSRU"%><%@page import="java.util.Collection"%>
<sru:explainResponse xmlns:sru="http://www.loc.gov/zing/srw/"
	                   xmlns:zr="http://explain.z3950.org/dtd/2.0/"
                     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

	<sru:version><%= request.getAttribute("version") %></sru:version>
	<sru:record>
		<sru:recordPacking>XML</sru:recordPacking>
		<sru:recordSchema>http://explain.z3950.org/dtd/2.0/</sru:recordSchema>
    
		<sru:recordData>
			<zr:explain>
				
        <zr:serverInfo protocol="SRU" version="1.1" transport="http" method="GET POST">
					<zr:host><%= request.getAttribute("host") %></zr:host>
					<zr:port><%= request.getAttribute("port") %></zr:port>
					<zr:database><%= request.getAttribute("database") %></zr:database>
				</zr:serverInfo>
				
        <zr:databaseInfo>
					<zr:title lang="en" primary="true">DWD SRU Interface</zr:title>
					<zr:description lang="en" primary="true">DWD SRU Interface</zr:description>
				</zr:databaseInfo>
        
				<zr:indexInfo>
          <zr:set name="dc" identifier="info:srw/cql-context-set/1/dc-v1.1"/>
          <zr:set name="cql" identifier="info:srw/cql-context-set/1/cql-v1.2"/>
          <zr:set name="geo" identifier="http://??"/>
          <zr:set name="gils" identifier="info:srw/cql-context-set/14/gils-v1.0"/>
          <zr:set name="rec" identifier="info:srw/cql-context-set/2/rec-1.1"/>
          
					<% Collection<String> attribs = (Collection<String>)request.getAttribute("supportedAttributes");  
					for ( String attrib :  attribs ) { %>
					<zr:index>
						<zr:map>
							<zr:name set="cql"><%= attrib%></zr:name>
						</zr:map>
					</zr:index>
					<% } %>
				</zr:indexInfo>
        
        <zr:schemaInfo>
          <zr:schema name="xml:meta" identifier="http://www.iso.org/iso/catalogue_detail.htm?csnumber=32557">
            <zr:title>ISO 19139</zr:title>
          </zr:schema>
        </zr:schemaInfo>
        
				<zr:configInfo>
					<zr:default type="numberOfRecords"><%= request.getAttribute("numberOfRecords") %></zr:default>
					<zr:setting type="maximumRecords"><%= request.getAttribute("maximumRecords") %></zr:setting>

				</zr:configInfo>
        
				<zr:metaInfo>
          <aggregatedFrom>DWD SRU Interface</aggregatedFrom>
					<dateModified>2010-08-31</dateModified>
				</zr:metaInfo>
        
			</zr:explain>
		</sru:recordData>
	</sru:record>

  <%@include file="diagnostic.jspf"%>
       
</sru:explainResponse>