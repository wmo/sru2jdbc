<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet [
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:gco="http://www.isotc211.org/2005/gco"
		xmlns:gmd="http://www.isotc211.org/2005/gmd"
		xmlns:gml="http://www.opengis.net/gml"
		xmlns:xlink="http://www.w3.org/1999/xlink"
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns="http://www.w3.org/1999/xhtml"
		exclude-result-prefixes="gco gmd gml xlink xsi">

  <!--   
       XSLT stylesheet for generating a synopsis of ISO 19139 metadata
       as XHTML fragment.

       Author(s):
         alexander.rettig@askvisual.de
         albert.brotzer@askvisual.de
       ask - Innovative Visualisierungsloesungen GmbH, Germany

       Notes:

       * see GISC Java class 'de.dwd.metadata.IfcXPath' for original
         definition of the XPath expressions referenced as 'XP_*' in
         the comment lines below.
  -->

  <!-- ============================== Imports =============================== -->

  <!-- =============================== Config =============================== -->

  <xsl:output method="xml"
              indent="yes" />

  <!-- ========================== Global Variables ========================== -->

  <xsl:variable name="gisc_baseUrl">
    <xsl:text>http://gisc.dwd.de/GISC_DWD</xsl:text>
  </xsl:variable>

  <xsl:variable name="gisc_retrieveDataUrl">
    <xsl:value-of select="$gisc_baseUrl"/><xsl:text>/retrieve_data.do</xsl:text>
  </xsl:variable>

  <!-- ============================= Templates ============================== -->

  <!-- ====================================================================== -->
  <!-- ==                          Synopsis Root                           == -->
  <!-- ====================================================================== -->

  <xsl:template name="MD_Metadata-Synopsis">

    <xsl:variable name="gisc_toplevel">
      <xsl:choose>
        <!-- XP_PARENT -->
        <xsl:when test="gmd:parentIdentifier">
          <xsl:text>toplevel=false</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>toplevel=true</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="gisc_pidpat">
      <xsl:text>pidpat=</xsl:text>
      <!-- XP_FILEIDENTIFIER -->
      <xsl:value-of select="normalize-space(gmd:fileIdentifier)"/>
    </xsl:variable>

    <xsl:variable name="gisc_recordURL">
      <xsl:value-of select="$gisc_retrieveDataUrl"/>
      <xsl:text>?</xsl:text>
      <xsl:value-of select="$gisc_pidpat"/>   
      <xsl:text>&amp;</xsl:text>
      <xsl:value-of select="normalize-space($gisc_toplevel)"/>
    </xsl:variable>

    <table class="list" width="100%">
      <tr class="odd">
	<td>PID</td>
	<td>
	  <!-- XP_FILEIDENTIFIER -->
	  <xsl:value-of select="gmd:fileIdentifier"/>
	</td>
      </tr>
      <tr class="even">
	<td>Title</td>
	<td>
	  <!-- XP_TITLE -->
	  <xsl:value-of select="gmd:identificationInfo/gmd:MD_DataIdentification/gmd:citation/gmd:CI_Citation/gmd:title" />
	</td>
      </tr>
      <tr class="odd">
	<td>Originator</td>
	<td>
	  <!-- XP_ORIGIN_ORG -->
	  <xsl:value-of select="gmd:identificationInfo/gmd:MD_DataIdentification/gmd:pointOfContact/gmd:CI_ResponsibleParty/gmd:organisationName" />
	</td>
      </tr>
      <tr class="even">
	<td>Abstract</td>
	<td>
	  <!-- XP_ABSTRACT -->
	  <xsl:value-of select="gmd:identificationInfo/gmd:MD_DataIdentification/gmd:abstract" />
	</td>
      </tr>
      <tr class="odd">
	<td>Code form</td>
	<td>
	  <!-- XP_CODE_NAME -->
	  <xsl:value-of select="gmd:distributionInfo/gmd:MD_Distribution/gmd:distributionFormat/gmd:MD_Format/gmd:name"/>
          <!-- XP_CODE_VERSION -->
	  (<xsl:value-of select="normalize-space(gmd:distributionInfo/gmd:MD_Distribution/gmd:distributionFormat/gmd:MD_Format/gmd:version)"/>)
	</td>
      </tr>            
      <tr class="even">
	<td>Web URL</td>
	<td>      
	<xsl:apply-templates select="gmd:distributionInfo/gmd:MD_Distribution/gmd:distributor/gmd:MD_Distributor/gmd:distributorTransferOptions/gmd:MD_DigitalTransferOptions/gmd:onLine"
			     mode="synopsis"/>
        <!-- This generates an artificial Link, which would allow to get back to a reference  to the single
             metadata set. It is not standard though and not thoroughly tested, whether the results are reliable
             thus it is commented out.
             <a href="{$gisc_recordURL}" target="_blank"><xsl:value-of select="$gisc_recordURL" /></a>
        -->
	</td>
      </tr>
      <tr class="odd">
	<td>Generated</td>
	<td>
	<!-- -->
	<xsl:value-of select="gmd:dateStamp/gco:DateTime/text()" />
	</td>
      </tr>
      <tr class="even">
        <td>Bounding box</td>
        <td>
          <table class="md-boundingBox">
            <tr>
              <td>
              </td>
              <td>
                <!-- XP_NORTH -->
                North: <xsl:value-of select="format-number(gmd:identificationInfo[1]/*/gmd:extent/*/gmd:geographicElement/*/gmd:northBoundLatitude, '##0.0000')"/>
              </td>
              <td>
              </td>
            </tr>
            <tr>
              <td>
                <!-- XP_WEST -->
                West:  <xsl:value-of select="format-number(gmd:identificationInfo[1]/*/gmd:extent/*/gmd:geographicElement/*/gmd:westBoundLongitude, '##0.0000')"/>
              </td>
              <td>
              </td>
              <td>
                <!-- XP_EAST -->
                East:  <xsl:value-of select="format-number(gmd:identificationInfo[1]/*/gmd:extent/*/gmd:geographicElement/*/gmd:eastBoundLongitude, '##0.0000')"/>
              </td>
            </tr>
            <tr>
              <td>
              </td>
              <td>
                <!-- XP_SOUTH -->
                South: <xsl:value-of select="format-number(gmd:identificationInfo[1]/*/gmd:extent/*/gmd:geographicElement/*/gmd:southBoundLatitude, '##0.0000')"/>
              </td>
              <td>
              </td>
            </tr>
          </table>
	</td>
      </tr>            
    </table>

  </xsl:template>

<!--  <xsl:template match="*gmd:distributorTransferOptions/gmd:MD_DigitalTransferOptions/gmd:online"-->
  <xsl:template match="gmd:distributorTransferOptions//gmd:onLine"
		mode="synopsis">
    <xsl:variable name="url">
      <xsl:value-of select="gmd:CI_OnlineResource/gmd:linkage/gmd:URL" />
    </xsl:variable>
    <a href="{$url}"><xsl:value-of select="normalize-space($url)"/></a>
    <br/>
  </xsl:template>

</xsl:stylesheet>