<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#160;"> ]>
<xsl:stylesheet version="1.0" 
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
      xmlns:xs="http://www.w3.org/2001/XMLSchema" 
      xmlns:fn="http://www.w3.org/2005/xpath-functions" 
      xmlns:gmd="http://www.isotc211.org/2005/gmd" 
      xmlns:gco="http://www.isotc211.org/2005/gco" 
      xmlns:gml="http://www.opengis.net/gml" 
      xmlns:sru="http://www.loc.gov/zing/srw/">
      
	<xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes"/>
	<!-- ###################################################################### 
		Wurzelknoten verarbeiten										 	
		###################################################################### -->
	<xsl:template match="/">
    <p style="margin-left: 2%; margin-right: 2%; margin-top: 2%; margin-bottom: 2%; font-family: 'Segoe UI', Tahoma, Arial, Helvetica, sans-serif;" >
      <h1>DWD Search/Retrieval via URL (SRU)</h1>
      <hr/>
      
      <table style="border: none">
        <tr>
          <td>Query:</td>
          <td style="font-weight: bold"><xsl:value-of select="/sru:searchRetrieveResponse/sru:echoedSearchRetrieveRequest/sru:query" /></td>
        </tr>                           
        <tr>
          <td>Total number of records:</td>
          <td style="font-weight: bold"><xsl:value-of select="/sru:searchRetrieveResponse/sru:numberOfRecords"/></td>
        </tr>                           
        <tr>
          <td>Number of records per page:</td>
          <td style="font-weight: bold"><xsl:value-of select="/sru:searchRetrieveResponse/sru:echoedSearchRetrieveRequest//sru:maximumRecords"/></td>
        </tr>                           
        <tr>
          <td>Start record:</td>
          <td style="font-weight: bold"><xsl:value-of select="/sru:searchRetrieveResponse/sru:echoedSearchRetrieveRequest//sru:startRecord"/></td>
        </tr>                           
      </table>
      <hr/>
  		<xsl:call-template name="records"/>
    </p>
	</xsl:template>
  
	<xsl:template name="records">
		<xsl:for-each select="/sru:searchRetrieveResponse/sru:records/sru:record">
			<p>
			<xsl:call-template name="record">
				<xsl:with-param name="clob" select="./sru:recordData"/>				
			</xsl:call-template>
			</p>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="record">
		<xsl:param name="clob"/>
		<!--
		<xsl:value-of select="$clob" />
		<p><xsl:text>clob</xsl:text><xsl:value-of select="$clob" /></p>
		-->
    <xsl:call-template name="title">
      <xsl:with-param name="title" select="./sru:recordData//gmd:identificationInfo/gmd:MD_DataIdentification/gmd:citation/gmd:CI_Citation/gmd:title/gco:CharacterString"/>
      <xsl:with-param name="onlineResource" select="./sru:recordData//gmd:distributionInfo/*/gmd:transferOptions/*/gmd:onLine/gmd:CI_OnlineResource/gmd:linkage/gmd:URL"/>
    </xsl:call-template>
    <br/>
    <xsl:call-template name="abstract">
      <xsl:with-param name="abstract" select="./sru:recordData//gmd:identificationInfo/gmd:MD_DataIdentification/gmd:abstract/gco:CharacterString"/>
    </xsl:call-template>
      <table style="border: none">
      <tr style="font-weight: bold">
        <td>
          <xsl:call-template name="identifier">
            <xsl:with-param name="identifier" select="./sru:recordData//gmd:MD_Metadata/gmd:fileIdentifier/gco:CharacterString/text()"/>
          </xsl:call-template>
        </td>
        <td>
          <xsl:call-template name="datestamp">
            <xsl:with-param name="datestamp" select="./sru:recordData/gmd:MD_Metadata/gmd:dateStamp/gco:DateTime"/>
          </xsl:call-template>
        </td>
      </tr>                           
    </table>
	</xsl:template>

  
  <xsl:template name="abstract">
    <xsl:param name="abstract"/>
      <font size="-1">
        <xsl:value-of select="$abstract"/>
      </font>
  </xsl:template>
  
	<xsl:template name="title">
		<xsl:param name="title"/>
		<xsl:param name="onlineResource"/>
      <xsl:choose>
        <xsl:when test="string-length($onlineResource) &gt; 0">
          <a>
            <xsl:attribute name="href"><xsl:value-of select="$onlineResource"/></xsl:attribute>
            <xsl:value-of select="$title"/>
          </a>
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="$title"/>
        </xsl:otherwise>
      </xsl:choose>
	</xsl:template>
  
	<xsl:template name="identifier">
		<xsl:param name="identifier"/>
		<font color="green" size="-2">
			<xsl:value-of select="$identifier"/>
		</font>
	</xsl:template>
	<xsl:template name="datestamp">
		<xsl:param name="datestamp"/>
		<font color="gray" size="-2">
			<xsl:value-of select="$datestamp"/>
		</font>
	</xsl:template>
</xsl:stylesheet>
