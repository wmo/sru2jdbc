<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#160;"> ]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:dwd="http://gisc.dwd.de/sru"
		xmlns:sru="http://www.loc.gov/zing/srw/"
		xmlns:xlink="http://www.w3.org/1999/xlink"
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns="http://www.w3.org/1999/xhtml"
		exclude-result-prefixes="dwd sru xlink xsi"
		>

  <!--   
       XSLT stylesheet for converting SRU response XML document to XHTML page.

       Author(s):
         alexander.rettig@askvisual.de
         albert.brotzer@askvisual.de
       ask - Innovative Visualisierungsloesungen GmbH, Germany

       Notes:

       * We propose/assume a namespace "http://gisc.dwd.de/sru"
         (prefix dwd:) for DWD specific extensions
  -->


  <!-- ============================== Imports =============================== -->

  <xsl:import href="dwd-sru-diagnostics.xsl" />
  <xsl:import href="dwd-pager.xsl" />
  <xsl:import href="dwd-iso19139.xsl" />

  <!-- =============================== Config =============================== -->

  <xsl:output method="xml"
	      version="1.0"
              indent="yes"
	      doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	      doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	      media-type="application/xhtml+xml"
	      />

  <!-- ========================== Global Variables ========================== -->

  <!-- ****************************** CAUTION *******************************
       this stylesheet is not suited to be imported in other stylesheets b/c
       of it's use of global variables!
  -->

  <xsl:variable name="_scripts">
    <script type="text/javascript" src="javascript/gisc_dwd_sru.js"></script>
    <!-- additionally needed script inclusions (may) go here -->
  </xsl:variable>

  <xsl:variable name="pageTitle">
    DWD Search/Retrieval via URL (SRU)
  </xsl:variable>

  
  <!-- Information contained in SRU response document according to the SRU standard.
       Variable names have the prefix "sru_"
  -->

  <!-- baseUrl in echoedSearchRetieveRequest is defined in SRU 1.2 but
       not in SRU 1.1.  In the latter case it may be provided by the
       DWD extraRequestData 'dwd:baseUrl' -->
  <!-- Opera doesn't allow local variables in variable templates, thus
       _sru_baseUrl must be defined globally too :-( -->
  <xsl:variable name="_sru_baseUrl" select="normalize-space(/sru:searchRetrieveResponse/sru:echoedSearchRetrieveRequest/sru:baseUrl|/sru:searchRetrieveResponse/sru:echoedSearchRetrieveRequest/sru:extraRequestData/*[local-name()='baseUrl'])"/>
  <xsl:variable name="sru_baseUrl">    
    <xsl:choose>
      <xsl:when test="$_sru_baseUrl">
	<xsl:value-of select="$_sru_baseUrl" />
      </xsl:when>
      <xsl:otherwise>
	<xsl:text>http://gisc.dwd.de/SRU2JDBC/</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="sru_numberOfRecords">
    <xsl:value-of select="/sru:searchRetrieveResponse/sru:numberOfRecords"/>
  </xsl:variable>

  <xsl:variable name="sru_startRecord">
    <xsl:value-of select="/sru:searchRetrieveResponse/sru:echoedSearchRetrieveRequest/sru:startRecord"/>
  </xsl:variable>

  <xsl:variable name="sru_maximumRecords">
    <xsl:value-of select="/sru:searchRetrieveResponse/sru:echoedSearchRetrieveRequest/sru:maximumRecords"/>
  </xsl:variable>

  <xsl:variable name="sru_recordPosition_begin">
    <xsl:value-of select="(/sru:searchRetrieveResponse/sru:records/sru:record/sru:recordPosition)[1]"/>
  </xsl:variable>

  <xsl:variable name="sru_recordPosition_end">
    <xsl:value-of select="(/sru:searchRetrieveResponse/sru:records/sru:record/sru:recordPosition)[last()]"/>
  </xsl:variable>

  <xsl:variable name="sru_nextRecordPosition">
    <xsl:value-of select="/sru:searchRetrieveResponse/sru:nextRecordPosition"/>
  </xsl:variable>

  <xsl:variable name="sru_query">
    <xsl:value-of select="normalize-space(/sru:searchRetrieveResponse/sru:echoedSearchRetrieveRequest/sru:query)"/>
  </xsl:variable>

  <xsl:variable name="sru_version">
    <xsl:value-of select="normalize-space(/sru:searchRetrieveResponse//sru:version)"/>
  </xsl:variable>

  <xsl:variable name="sru_recordSchema">
    <xsl:value-of select="normalize-space(/sru:searchRetrieveResponse/sru:echoedSearchRetrieveRequest/sru:recordSchema)"/>
  </xsl:variable>
  
  <xsl:variable name="sru_resultSetId">
    <xsl:value-of select="normalize-space(/sru:searchRetrieveResponse/sru:resultSetId)"/>
  </xsl:variable>

  <!-- Opera doesn't allow local variables in variable templates, thus
       l_sru_stylesheet must be defined globally too :-( -->
  <xsl:variable name="l_sru_stylesheet" select="normalize-space(/sru:searchRetrieveResponse/sru:echoedSearchRetrieveRequest/sru:stylesheet)"/>
  <xsl:variable name="sru_stylesheet">
    <xsl:choose>
      <xsl:when test="$l_sru_stylesheet">
	<xsl:value-of select="$l_sru_stylesheet" />
      </xsl:when>
      <xsl:otherwise>
	<xsl:text>xsl/dwd-sru.xsl</xsl:text> <!-- the path of this file relative to the sru_baseUrl -->
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <!-- Information contained in SRU response document in extra(Request)Data according
       to DWD proposal.
       Variable names have the prefix "dwd_"
  -->

  <xsl:variable name="dwd_stylesheetDetailLevel">
     <xsl:value-of select="normalize-space(/sru:searchRetrieveResponse/sru:echoedSearchRetrieveRequest/sru:extraRequestData/*[local-name()='stylesheetDetailLevel'])"/>
  </xsl:variable>

  <xsl:variable name="dwd_lang">
     <xsl:value-of select="normalize-space(/sru:searchRetrieveResponse/sru:echoedSearchRetrieveRequest/sru:extraRequestData/*[local-name()='lang'])"/>
  </xsl:variable>

  <!-- Set up major parts of an DWD GISC search/retrieve URL for 'repeating' the search -->
  <!-- 
       CAUTION: only available parameters are included here. This may
       result into incomplete search/request URLs 
  -->
  <xsl:variable name="sru_searchRetrieveURL">
    <xsl:value-of select="$sru_baseUrl"/>
    <xsl:text>sru?operation=searchRetrieve</xsl:text>
    <xsl:if                                                               test  ="$sru_query">
      <xsl:text>&amp;query=</xsl:text>                      <xsl:value-of select="$sru_query"/>
    </xsl:if>
    <xsl:if                                                               test  ="$sru_version">
      <xsl:text>&amp;version=</xsl:text>                    <xsl:value-of select="$sru_version"/>
    </xsl:if>
    <xsl:if                                                               test  ="$sru_recordSchema">
      <xsl:text>&amp;recordSchema=</xsl:text>               <xsl:value-of select="$sru_recordSchema"/>
    </xsl:if>
    <xsl:if                                                               test  ="$sru_maximumRecords">
      <xsl:text>&amp;maximumRecords=</xsl:text>             <xsl:value-of select="$sru_maximumRecords"/>
    </xsl:if>
    <xsl:if                                                               test  ="$sru_stylesheet">
      <xsl:text>&amp;stylesheet=</xsl:text>                 <xsl:value-of select="$sru_stylesheet"/>
    </xsl:if>
    <!-- 'x-dwd-resultSetId': oddly enough there is no SRU standard way to
	 send the resultSetId search/retrieve request to refer to a previously 
	 generated result set.
	 We add this capability via DWD extraRequestData 'dwd:resultSetId'
    -->
    <xsl:if                                                               test  ="$sru_resultSetId">
      <xsl:text>&amp;x-dwd-resultSetId=</xsl:text>          <xsl:value-of select="$sru_resultSetId"/>
    </xsl:if>
    <xsl:if                                                               test  ="$dwd_stylesheetDetailLevel">
      <xsl:text>&amp;x-dwd-stylesheetDetailLevel=</xsl:text><xsl:value-of select="$dwd_stylesheetDetailLevel"/>
    </xsl:if>
    <xsl:if                                                               test  ="$dwd_lang">
      <xsl:text>&amp;x-dwd-lang=</xsl:text>                 <xsl:value-of select="$dwd_lang"/>
    </xsl:if>
  </xsl:variable>

  <!-- ============================= Templates ============================== -->

  <!-- ====================================================================== -->
  <!-- ==                             SRU Root                             == -->
  <!-- ====================================================================== -->
  <xsl:template match="/">
    <xsl:call-template name="GISC_DWD">
      <xsl:with-param name="_scripts" select="$_scripts"/>
      <xsl:with-param name="pageTitle" select="$pageTitle"/>
    </xsl:call-template>
  </xsl:template>

  <!-- ========================== Named Templates =========================== -->

  <!-- ====================================================================== -->
  <!-- ==                  Set up the HTML frame document                  == -->
  <!-- ====================================================================== -->
  <xsl:template name="GISC_DWD">

    <xsl:param name="pageTitle">
      GISC DWD
    </xsl:param>
    <xsl:param name="_scripts"/>

    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
	<link type="text/css" rel="stylesheet" href="css/GISC_DWD_SRU.css"/>
<!-- TODO: does not work in IE 8 (all other browsers work):
        <xsl:copy-of select="$_scripts"/>
 thus we put it in here directly:
-->
	<script type="text/javascript" src="javascript/gisc_dwd_sru.js"></script>

	<title><xsl:value-of select="$pageTitle"/></title>

      </head>
      <body>
	<div id="gisc">
	  <div id="DWD_Content">
            <!-- this link is currently not necessary
            <a href="{$sru_baseUrl}" style="font-size:smaller;">
              <xsl:text>&lt;&lt;&lt; Back to Search</xsl:text>
            </a>
	    -->
	    <h1><xsl:value-of select="$pageTitle"/></h1>

	    <xsl:apply-templates/>

	  </div> <!-- DWD_Content -->
	</div> <!-- gisc -->
      </body>
    </html>

  </xsl:template> <!-- name="GISC_DWD" -->


  
  <!-- ====================================================================== -->
  <!-- ==                          SRU container                           == -->
  <!-- ====================================================================== -->

  <xsl:template match="/sru:searchRetrieveResponse">
    
    <table>
      <tbody>
        <tr>
          <td><span class="label">Query:&nbsp;</span></td> <!-- TODO: use translation mechanism -->
          <td><xsl:value-of select="$sru_query"/></td>
        </tr>
        <!-- Currently we don't want to show the numberOfRecords, as the 'pager' (imported from dwd-pager.xsl) shows it too
        <tr>
          <th><span class="label">Records Found</span></th> <!- TODO: use translation mechanism ->
          <td><xsl:value-of select="$sru_numberOfRecords"/></td>
        </tr>
        -->
        <!-- Currently we don't want to show the ResultSetId which has no useful meaning for the reader
        <tr>
          <th><span class="label">Result Set Identifier</span></th> <!- TODO: use translation mechanism ->
          <td><xsl:value-of select="$sru_resultSetId"/></td>
        </tr>
        -->
      </tbody>
    </table>
    
    <xsl:call-template name="pager">
      <xsl:with-param name="numberOfRecords"    select="$sru_numberOfRecords"/>
      <xsl:with-param name="startRecord"        select="$sru_startRecord"/>
      <xsl:with-param name="maximumRecords"     select="$sru_maximumRecords"/>
      <xsl:with-param name="nextRecordPosition" select="$sru_nextRecordPosition"/>
      <xsl:with-param name="beginRecord"        select="$sru_recordPosition_begin"/>
      <xsl:with-param name="endRecord"          select="$sru_recordPosition_end"/>
    </xsl:call-template>

    <hr/>
    
    <xsl:for-each select="sru:diagnostics">
      <xsl:call-template name="sru:diagnostics" />
    </xsl:for-each>

    <xsl:for-each select="sru:records/sru:record">
      <xsl:call-template name="sru:record"/>
    </xsl:for-each>

    <hr/>
    <xsl:call-template name="pager">
      <xsl:with-param name="numberOfRecords"    select="$sru_numberOfRecords"/>
      <xsl:with-param name="startRecord"        select="$sru_startRecord"/>
      <xsl:with-param name="maximumRecords"     select="$sru_maximumRecords"/>
      <xsl:with-param name="nextRecordPosition" select="$sru_nextRecordPosition"/>
      <xsl:with-param name="beginRecord"        select="$sru_recordPosition_begin"/>
      <xsl:with-param name="endRecord"          select="$sru_recordPosition_end"/>
    </xsl:call-template>

  </xsl:template>

  <!-- ====================================================================== -->
  <!--                               Record Information                       -->
  <!-- ====================================================================== -->
  <xsl:template name="sru:record" match="sru:record">

    <xsl:variable name="recordPosition" select="number(sru:recordPosition)"/>

    <div class="sru-record-info">
      <xsl:text>Record </xsl:text>
      <xsl:value-of select="$recordPosition"/>
      <xsl:text> of </xsl:text>
      <xsl:value-of select="$sru_numberOfRecords"/>
    </div>

    <div class="sru-record-wrapper" >
      <xsl:for-each select="sru:recordData">
	<xsl:apply-templates select="*" mode="root">
	  <!-- mode="root" enforces that only entry point templates of imported
	       stylesheets are matching -->
	  <xsl:with-param name="detailLevel" select="$dwd_stylesheetDetailLevel" />
	  <xsl:with-param name="lang" select="$dwd_lang" />
	</xsl:apply-templates>
      </xsl:for-each>
    </div>

  </xsl:template>

  <!-- ==================== echoedSearchRetrieveRequest ===================== -->
  <xsl:template match="/sru:searchRetrieveResponse/sru:echoedSearchRetrieveRequest">
    <!-- Swallow, relevant info is already extracted globally -->
  </xsl:template>
    

</xsl:stylesheet>
