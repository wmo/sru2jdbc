<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet [
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:sru="http://www.loc.gov/zing/srw/"
		xmlns:diag="http://www.loc.gov/zing/srw/diagnostic/"
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns="http://www.w3.org/1999/xhtml"
		exclude-result-prefixes="sru diag xsi"
		>
  <!--   
       XSLT stylesheet for converting SRU response XML document
       diagnostic parts to XHTML fragment.

       Author(s):
         alexander.rettig@askvisual.de
         albert.brotzer@askvisual.de
       ask - Innovative Visualisierungsloesungen GmbH, Germany

       Notes:
  -->

<!--  <xsl:template match="*//gmd:distributorTransferOptions/gmd:MD_DigitalTransferOptions/gmd:online"-->
  <xsl:template name="sru:diagnostics" match="*//sru:diagnostics" >
    <xsl:apply-templates />
  </xsl:template>

  <!-- Surrogate diagnostics: 'cast away' the mode set for entry point of imported stylesheet(s) -->
  <xsl:template match="diag:diagnostic" mode="root" >
    <xsl:call-template name="diag:diagnostic" />
  </xsl:template>

  <xsl:template name="diag:diagnostic" match="diag:diagnostic" >
    <table class="diagiagnostics">
      <caption>
        Diagnostics
      </caption>
      <tbody>
	<xsl:apply-templates />
      </tbody>
    </table>
  </xsl:template>

  <xsl:template match="diag:diagnostic/*">
    <tr>
      <th>
	<xsl:value-of select="name(.)"/>
      </th>
      <td>
	<xsl:value-of select="."/>
      </td>
    </tr>
  </xsl:template>

</xsl:stylesheet>