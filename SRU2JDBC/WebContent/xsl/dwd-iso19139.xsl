<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#160;"> ]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:gco="http://www.isotc211.org/2005/gco"
		xmlns:gmd="http://www.isotc211.org/2005/gmd"
		xmlns:gmx="http://www.isotc211.org/2005/gmx"
		xmlns:gml="http://www.opengis.net/gml"
		xmlns:xlink="http://www.w3.org/1999/xlink"
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		xmlns="http://www.w3.org/1999/xhtml"
		exclude-result-prefixes="gco gmd gml xlink xsi"
		>
  <!--   
       XSLT stylesheet for converting ISO 19139 metadata XML document
       to XHTML fragment.

       Author(s):
         alexander.rettig@askvisual.de
         albert.brotzer@askvisual.de
       ask - Innovative Visualisierungsloesungen GmbH, Germany

       Notes:

       * Search for 'DEACTIVATED' in comments to find currently - um -
         deactivated features
  -->

  <!-- ============================== Imports =============================== -->

  <xsl:import href="dwd-iso19139-synopsis.xsl" />

  <!-- =============================== Config =============================== -->

  <xsl:output method="xml"
	      indent="yes" />

  <!-- ================================ Keys ================================ -->

  <xsl:key name="element-descriptions" 
	   match="*[local-name()='element']"
	   use="@name"/>

  <!-- ========================== Global Variables ========================== -->

  <xsl:variable name="level" select="0" />

  <!-- ============================= Templates ============================== -->

  <!-- ====================================================================== -->
  <!-- ==                          Metadata Root                           == -->
  <!-- ====================================================================== -->


  <xsl:template name="MD_Metadata" match="*//gmd:MD_Metadata" mode="root">
    <xsl:param name="detailLevel" select="0" />
    <xsl:param name="lang"/> <!-- implictly defaults to 'en' -->

    <div class="md-title">
      <!-- XP_TITLE -->
      <xsl:value-of select="gmd:identificationInfo/gmd:MD_DataIdentification/gmd:citation/gmd:CI_Citation/gmd:title"/>
    </div>

    <div class="md-abstract">
      <!-- XP_ABSTRACT -->
      <xsl:value-of select="gmd:identificationInfo/gmd:MD_DataIdentification/gmd:abstract"/>
    </div>

    <xsl:if test="number($detailLevel) >= 1 ">
      <xsl:variable name="_id" select="concat('synopsis_',string(generate-id()))"/>
      <fieldset class="md-fieldset level-{$level}" id="{$_id}">
	<legend class="md-legend">
	  <span class="toggleDisclosure-button" onclick="toggleDisclosure('{$_id}')">&nbsp;</span> <!-- &nbsp; needed for IE 8 / MSXML -->
	  <span class="label"> Synopsis</span> <!-- TODO: do we need another class here?!?!? -->
	</legend>
	<div class="md-fieldset-content">
	  <xsl:call-template name="MD_Metadata-Synopsis" />
	</div>
      </fieldset>
    </xsl:if>

    <xsl:if test="$detailLevel >= 2 ">
      <xsl:call-template name="fieldset"/>
    </xsl:if>

  </xsl:template>


  <!-- ====================================================================== -->
  <!-- ==                       Recursive Processing                       == -->
  <!-- ====================================================================== -->

  <xsl:template name="fieldset">
    <xsl:param name="level" select="1"/>

    <!-- 
	 Unique node ID is generated by counting preceding and
	 ancestor nodes which results into the number of this node in
	 the input document
	 <xsl:variable name="_id" select="count(preceding::*) + count( ancestor::*)"/>
    -->    
    <xsl:variable name="_id" select="generate-id()"/>
    
    <fieldset class="md-fieldset level-{$level} closed">
      <xsl:attribute name="id">
	<xsl:value-of select="$_id"/>
      </xsl:attribute>
      <xsl:apply-templates select="attribute::*"/>
      <legend class="md-legend">
	<span class="toggleDisclosure-button" onclick="toggleDisclosure('{$_id}')">&nbsp;</span> <!-- &nbsp; needed for IE 8 / MSXML -->
	<xsl:text> </xsl:text>
	<!-- Insert deep disclosure button for nodes which have a subtree of depth > 1 -->
	<xsl:if test="count(*[namespace-uri()='http://www.isotc211.org/2005/gmd'][count(descendant::*[namespace-uri()='http://www.isotc211.org/2005/gmd'])>0])>0">
	  <span class="toggleDeepDisclosure-button" onclick="toggleDeepDisclosure('{$_id}')">&nbsp;</span> <!-- &nbsp; needed for IE 8 / MSXML -->
	  <xsl:text> </xsl:text>
	</xsl:if>
	<span class="label" onclick="toolTip(this.id)" id="{name()}_{$_id}">
	  <xsl:call-template name="label-element"/>
	</span>
      </legend>
      <div class="md-fieldset-content"> 
	<table class="md-table">
	  <tbody>
	    <xsl:apply-templates>
	      <xsl:with-param name="level" select="$level + 1"/>
	    </xsl:apply-templates>
	  </tbody>
	</table>
      </div>
    </fieldset>

  </xsl:template>


  <!-- 
       One simple item entry using one table row, 
       * if given, 'title' is "translated"
       * if no 'title' is given, the content spans both columns of the containing table
       * 'content' is not "translated"
  -->
  <xsl:template name="simpleItem">    
    <xsl:param name="class" select="'md-item'"/>
    <xsl:param name="title" />
    <xsl:param name="content"/>
    <tr>
      <xsl:if test="$class!=''">
	<xsl:attribute name="class">
	  <xsl:value-of select="$class"/>
	</xsl:attribute>
      </xsl:if>

      <xsl:choose>

	<xsl:when test="$title">
	  <th>
	    <!--	<xsl:apply-templates mode="translate" select="$title"/> -->
	    <xsl:call-template name="translate">
	      <xsl:with-param name="from">
		<xsl:copy-of select="$title"/>
	      </xsl:with-param>
	    </xsl:call-template>
	    <!-- <xsl:value-of select="$title"/> -->
	  </th>
	  <td>
	    <xsl:copy-of select="$content"/>
	  </td>
	</xsl:when>

	<xsl:otherwise>
	  <td colspan="2">
	    <xsl:copy-of select="$content"/>
	  </td>
	</xsl:otherwise>

      </xsl:choose>
    </tr>
  </xsl:template>

  <!-- ======================================================================== -->
  <!--                  Catch nodes from the namespace "gmd:"                   -->

  <!-- TODO: level-Zaehlung tut so nicht. Muesste bei allen
       template-aufrufen durchgereicht werden :-(
  -->
  <xsl:template match="*[namespace-uri()='http://www.isotc211.org/2005/gmd' or namespace-uri()='http://www.isotc211.org/2005/gmx']">
    <xsl:param name="level" select="0" />
    <tr>
      <td colspan="2">
	
	<xsl:call-template name="fieldset">
	  <xsl:with-param name="level" select="$level + 1"/>
	</xsl:call-template>

      </td> 
    </tr>
  </xsl:template>


  <!-- ======================================================================== -->
  <!--                  All elements with attribute 'codeList'                  -->

  <!-- TODO: it's assumed, that these nodes have no child nodes except for
       text() children!  

       This has to be verfied!
  -->
  <xsl:template match="*[count(*)=1][local-name(*/@*)='codeList']" priority="1">
    <xsl:call-template name="simpleItem">
      <xsl:with-param name="class" select="'md-codeList'"/>
      <xsl:with-param name="title" select="name()"/>
      <xsl:with-param name="content">
	<xsl:call-template name="codeList"/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="codeList">
    <a href="{*/@codeList}">
      <xsl:choose>
	<xsl:when test="text()">
	  <xsl:value-of select="."/>
	  <xsl:if test="normalize-space(string(.)) != normalize-space(string(*/@codeListValue))">
	    <xsl:text> (</xsl:text><xsl:value-of select="normalize-space(*/@codeListValue)"/><xsl:text>)</xsl:text>
	  </xsl:if>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:value-of select="normalize-space(*/@codeListValue)"/>
	</xsl:otherwise>
      </xsl:choose>
    </a>
  </xsl:template>

  <!-- ======================================================================== -->
  <!--       All gmd: elements, which have no other gmd: elements as descendants -->
  <!-- TODO: Optimization potential:

       If the namespaces are strictly hierarchical, thus there's no
       namespace other than 'gmd:' which can have 'gmd:' children,
       then the XPath expression can be the following instead of the
       currently used one:
       *[namespace-uri()='http://www.isotc211.org/2005/gmd'][count(*[namespace-uri()='http://www.isotc211.org/2005/gmd'])=0]
       It should be faster to be processed.
  -->
  <xsl:template match="*[namespace-uri()='http://www.isotc211.org/2005/gmd'][count(descendant::*[namespace-uri()='http://www.isotc211.org/2005/gmd'])=0]">

    <xsl:call-template name="simpleItem">
      <xsl:with-param name="title" select="name()"/>
      <xsl:with-param name="content">
	<xsl:if test="normalize-space(.)">
	  <xsl:value-of select="."/>
	  <br/>          
	</xsl:if>
	<xsl:apply-templates select="attribute::*"/> <!-- TODO -->
      </xsl:with-param>
    </xsl:call-template>

  </xsl:template>

  <!-- ======================================================================== -->
  <!-- All attributes (if not caught explicitly by other rules) -->
  <!-- TODO: attributes of non-leaf elements seem to be not too
       interesting, maybe we should swallow them
  -->
  <!--
      Not working in xsltproc (Safari/Chrome) although correct XPath 1.0 expression:
      <xsl:template match="attribute::*">
  -->
  <xsl:template match="@*">
    <xsl:if test="position()>1">
      <br/>
    </xsl:if>
    <span class="md-attribute"><xsl:value-of select="name()"/> = <xsl:value-of select="."/></span>
  </xsl:template>

  <!-- ======================================================================== -->
  <!-- == Special Attributes == -->
  
  <!-- swallow these completely -->
  <!--
      Not working in xsltproc (Safari/Chrome) although correct XPath 1.0 expression:
      <xsl:template match="attribute::xsi:schemaLocation|attribute::id">
  -->
  <xsl:template match="@xsi:schemaLocation|@id">
  </xsl:template>


  <!-- ======================================================================== -->
  <!-- == Special Elements == -->
  
  <!-- Intermediate nodes, 'type descriptors': fold/hide them... -->
  <xsl:template match="*/gmd:*[contains(local-name(),'_') and local-name() != 'MD_TopicCategoryCode']"> <!-- FIXME: Sonderfaelle zerstoeren struktur -->
    <xsl:param name="level" select="0" />
    <xsl:apply-templates>
      <xsl:with-param name="level" select="$level + 1"/>
    </xsl:apply-templates>
  </xsl:template>

  <!-- Keywords -->
  <xsl:template name="gmd:MD_Keywords" match="*/gmd:MD_Keywords">
    <xsl:apply-templates select="*[not(local-name()='keyword')]"/>
    <xsl:call-template name="simpleItem">
      <xsl:with-param name="content">
	<xsl:for-each select="gmd:keyword">
	  <xsl:if test="position()>1">
	    <xsl:text>, </xsl:text>
	  </xsl:if>
	  <xsl:value-of select="normalize-space(.)"/>
	</xsl:for-each>       
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <!-- Resource Constraints -->
  <xsl:template match="*/gmd:MD_LegalConstraints">
    <xsl:apply-templates select="*[not(local-name()='accessConstraints' or local-name()='useConstraints')]"/>

    <xsl:call-template name="simpleItem">
      <xsl:with-param name="title" select="'accessConstraints'"/>
      <xsl:with-param name="content">
	<xsl:for-each select="gmd:accessConstraints">
	  <xsl:if test="position()>1">
	    <xsl:text>, </xsl:text>
	  </xsl:if>
	  <xsl:call-template name="codeList"/>
	</xsl:for-each>       
      </xsl:with-param>
    </xsl:call-template>

    <xsl:call-template name="simpleItem">
      <xsl:with-param name="title" select="'useConstraints'"/>
      <xsl:with-param name="content">
	<xsl:for-each select="gmd:useConstraints">
	  <xsl:if test="position()>1">
	    <xsl:text>, </xsl:text>
	  </xsl:if>
	  <xsl:call-template name="codeList"/>
	</xsl:for-each>       
      </xsl:with-param>
    </xsl:call-template>

  </xsl:template>

  <!-- E-Mail Address -->
  <xsl:template match="*/gmd:electronicMailAddress">
    <xsl:call-template name="simpleItem">
      <xsl:with-param name="title" select="name()"/>
      <xsl:with-param name="content">
	<a href="mailto:{normalize-space(.)}"><xsl:value-of select="normalize-space(.)"/></a>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <!-- URL -->
  <xsl:template match="*/gmd:URL">
    <xsl:call-template name="simpleItem">
      <xsl:with-param name="title" select="name()"/>
      <xsl:with-param name="content">
	<a href="{normalize-space(.)}"><xsl:value-of select="normalize-space(.)"/></a>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <!-- temporalElement --> <!-- TODO: ugly dealing with special case... -->
  <!-- TODO: this is prototypical -->
  <xsl:template name="gmd:EX_TemporalExtentxxx" match="*/gmd:EX_TemporalExtentxxx">
       <tr>
	 <td colspan="2">
	   <xsl:text>   [</xsl:text>
	   <xsl:value-of select="gmd:extent/gml:TimePeriod/gml:beginPosition" />
	   <xsl:text>&nbsp;&nbsp;-&nbsp;&nbsp;</xsl:text>
	   <xsl:value-of select="gmd:extent/gml:TimePeriod/gml:endPosition" />
	   <xsl:text>]&nbsp;&nbsp;&nbsp;&nbsp;</xsl:text>
	   <xsl:value-of select="local-name(gmd:extent/gml:TimePeriod/gml:timeInterval)" />
	   <xsl:text>=</xsl:text>
	   <xsl:value-of select="gmd:extent/gml:TimePeriod/gml:timeInterval" />
	   <!-- TODO: this was an unsuccessful try to intepret the Standard
		<xsl:text>&nbsp;*&nbsp;</xsl:text>
		<xsl:value-of select="gmd:extent/gml:TimePeriod/gml:timeInterval/@radix" />
		<xsl:text>^(</xsl:text>
		<xsl:value-of select="number(gmd:extent/gml:TimePeriod/gml:timeInterval/@factor) * (-1)" />
		<xsl:text>) </xsl:text>
		<xsl:value-of select="gmd:extent/gml:TimePeriod/gml:timeInterval/@unit" />
		<xsl:text></xsl:text>
	   -->
	   <xsl:text>&nbsp;(</xsl:text>
	   <xsl:value-of select="local-name(gmd:extent/gml:TimePeriod/gml:timeInterval/@radix)" />
	   <xsl:text>=</xsl:text>
	   <xsl:value-of select="gmd:extent/gml:TimePeriod/gml:timeInterval/@radix" />
	   <xsl:text>,&nbsp;</xsl:text>
	   <xsl:value-of select="local-name(gmd:extent/gml:TimePeriod/gml:timeInterval/@factor)" />
	   <xsl:text>=</xsl:text>
	   <xsl:value-of select="gmd:extent/gml:TimePeriod/gml:timeInterval/@factor" />
	   <xsl:text>,&nbsp;</xsl:text>
	   <xsl:value-of select="local-name(gmd:extent/gml:TimePeriod/gml:timeInterval/@unit)" />
	   <xsl:text>=</xsl:text>
	   <xsl:value-of select="gmd:extent/gml:TimePeriod/gml:timeInterval/@unit" />
	   <xsl:text>)</xsl:text>		
	 </td>
       </tr> 
  </xsl:template>

  <!-- geographicElement --> <!-- TODO: ugly dealing with special case... -->
  <xsl:template name="gmd:EX_GeographicBoundingBox" match="*/gmd:EX_GeographicBoundingBox">
    <tr>
      <td colspan="2">
	<table class="md-boundingBox">
	  <tbody>
	    <tr>
	      <td>
	      </td>
	      <td>
		<xsl:for-each select="gmd:northBoundLatitude[1]">
		  <strong>
		    <xsl:call-template name="label-element"/>
		  </strong>
		  <br/>
		  <xsl:value-of select="format-number(., '##0.0000')"/>
		</xsl:for-each>
	      </td>
	      <td>
	      </td>
	    </tr>
	    <tr>
	      <td>
		<xsl:for-each select="gmd:westBoundLongitude[1]">
		  <strong>
		    <xsl:call-template name="label-element"/>
		  </strong>
		  <br/>
		  <xsl:value-of select="format-number(., '##0.0000')"/>
		</xsl:for-each>
	      </td>
	      <td>
	      </td>
	      <td>
		<xsl:for-each select="gmd:eastBoundLongitude[1]">
		  <strong>
		    <xsl:call-template name="label-element"/>
		  </strong>
		  <br/>
		  <xsl:value-of select="format-number(., '##0.0000')"/>
		</xsl:for-each>
	      </td>
	    </tr>
	    <tr>
	      <td>
	      </td>
	      <td>
		<xsl:for-each select="gmd:southBoundLatitude[1]">
		  <strong>
		    <xsl:call-template name="label-element"/>
		  </strong>
		  <br/>
		  <xsl:value-of select="format-number(., '##0.0000')"/>
		</xsl:for-each>
	      </td>
	      <td>
	      </td>
	    </tr>
	  </tbody>
	</table>
      </td>
    </tr>
  </xsl:template>

  <xsl:template name="gmd:dimension" match="*/gmd:dimension">
    <xsl:call-template name="simpleItem">
      <xsl:with-param name="title" select="name()"/>
      <xsl:with-param name="content">
	[<xsl:value-of select="."/>]
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <!-- -->
  <!-- Swallow these
       <xsl:template match="*/gmd:onlineResource" />
       <xsl:template match="*/gmd:contentInfo" />
       <xsl:template match="*/gmd:distributorTransferOptions" />
       <xsl:template match="*/gmd:contactInfo" />
       <xsl:template match="*/gmd:citedResponsibleParty" />
       <xsl:template match="*/gmd:contentInfo" />
       <xsl:template match="*/gmd:dataQualityInfo" />
       <xsl:template match="*/gmd:distributionInfo" />
  -->


  <!-- ======================================================================== -->
  
  <!-- swallow empty subtrees: "empty" means, neither attributes nor text() nodes contained. -->
  <!-- TODO: currently they're not really swallowed but only marked by their class as such -->

<!-- DEACTIVATED
  <xsl:template match="*[count(attribute::*)=0][count(descendant::text())=0]">
    <div class="md-swallowed">
      SWALLOWED while empty: <strong><xsl:value-of select="name()"/></strong>
      <xsl:copy-of select="."/>
    </div>
  </xsl:template>
-->


  <!-- ====================================================================== -->
  <!-- ==                           Translation                            == -->
  <!-- ====================================================================== -->

  <!-- ====================================================================== -->
  <!-- ==                   Translate based on a string                    == -->
  <!-- 
       Advantage: can be used to translate strings not stemming from
       node name()s
  -->
  <xsl:template name="translate">

    <xsl:param name="from" select="''"/>

    <xsl:variable name="_translation">
      <xsl:for-each select="document('iso19139-descriptions.xml')">
	<xsl:value-of select="normalize-space(key('element-descriptions',normalize-space($from))/label)"/>
      </xsl:for-each>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$_translation != ''">
	<xsl:copy-of select="$_translation"/>
      </xsl:when>
      <!-- everything no translation is available for goes here... -->
      <xsl:otherwise>
	<xsl:copy-of select="$from"/> <!-- leave it as is -->
      </xsl:otherwise>
    </xsl:choose>

  </xsl:template>

  <!-- ====================================================================== -->
  <!-- ==     Translate based on the name() of the given node/nodeset      == -->
  <!--
      Advantage: proper namespace handling
  -->
  <xsl:template name="label-element">
    
    <xsl:variable name="_local-name" select="local-name()" />
    <xsl:variable name="_namespace-uri" select="namespace-uri()" />

    <xsl:variable name="_translation">
      <xsl:for-each select="document('iso19139-descriptions.xml')">
	<xsl:value-of select="key('element-descriptions',$_local-name)[namespace-uri() = $_namespace-uri]/label"/>
      </xsl:for-each>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$_translation != ''">
	<xsl:copy-of select="$_translation"/>
      </xsl:when>
      <!-- everything no translation is available for goes here... -->
      <xsl:otherwise>
	<xsl:value-of select="$_local-name"/> <!-- leave it as is -->
      </xsl:otherwise>
    </xsl:choose>

  </xsl:template>


  <!-- ====================================================================== -->
</xsl:stylesheet>
