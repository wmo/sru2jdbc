<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#160;"> ]>
<xsl:stylesheet version="1.0" 
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns="http://www.w3.org/1999/xhtml">

  <!--   
       XSLT stylesheet for converting SRU response XML document to
       XHTML fragment.

       Author(s):
         alexander.rettig@askvisual.de
         albert.brotzer@askvisual.de
       ask - Innovative Visualisierungsloesungen GmbH, Germany

       Notes:
  -->

  <!-- =============================== Config =============================== -->
  <xsl:output method="xml"
              indent="yes"/>

  <!-- ====================================================================== -->
  <!-- ==                              Pager                               == -->
  <!-- ====================================================================== -->
  <xsl:template name="pager">
    <xsl:param name="numberOfRecords" select="0" />
    <xsl:param name="startRecord"     select="1"/>
    <xsl:param name="maximumRecords"  select="10"/>
    <xsl:param name="nextRecordPosition" />
    <xsl:param name="beginRecord" />
    <xsl:param name="endRecord" />

    <xsl:variable name="prevStartRecord"     select="$startRecord - $maximumRecords"/>
    <xsl:variable name="nextStartRecord"     select="$nextRecordPosition"/>
    <xsl:variable name="lastPageStartRecord" select="floor(($numberOfRecords - 1) div $maximumRecords) * $maximumRecords + 1" />

    <table class="pager" width="100%" cellpadding="2" cellspacing="0" border="0">
      <tr>
	<td><span class="label">Matches:&nbsp;</span><xsl:value-of select="$numberOfRecords"/></td>
	<td align="right" style="width:1%; white-space:nowrap"> <!-- TODO: nowrap="yes" ersetzen durch xhtml konformes stylesheet entry -->

	  <xsl:if test="$numberOfRecords > 0">

	    <!-- ============= First page ============== -->
	    <xsl:choose>

	      <xsl:when test="$startRecord > 1">
		<span class="active">
		  <a href="{$sru_searchRetrieveURL}&amp;startRecord=1">
		    <xsl:text>First page</xsl:text>
		  </a>
		</span>
	      </xsl:when>

	      <xsl:otherwise>
		<span class="inactive">
		  <xsl:text>First page</xsl:text>
		</span>
	      </xsl:otherwise>

	    </xsl:choose>
	    <!-- ======================================= -->

	    <xsl:text>&nbsp;|&nbsp;</xsl:text>

	    <!-- ============ Previous Page ============ -->
	    <xsl:choose>

	      <xsl:when test="$prevStartRecord >= 1">
		<span class="active">
		  <a href="{$sru_searchRetrieveURL}&amp;startRecord={$prevStartRecord}">
		    <xsl:text>Previous&nbsp;</xsl:text>
		    <xsl:value-of select="$maximumRecords"/>
		  </a>
		</span>
	      </xsl:when>

	      <xsl:otherwise>
		<span class="inactive">
		  <xsl:text>Previous </xsl:text><xsl:value-of select="$maximumRecords"/>
		</span>
	      </xsl:otherwise>

	    </xsl:choose>
	    <!-- ======================================= -->

	    <xsl:text>&nbsp;|&nbsp;</xsl:text>

	    <!-- ============ Current Page ============= -->
	    <xsl:value-of select="$beginRecord"/>
	    <xsl:text> - </xsl:text>
	    <xsl:value-of select="$endRecord"/>
	    <xsl:text> of </xsl:text>
	    <xsl:value-of select="$numberOfRecords"/>
	    <!-- ======================================= -->

	    <xsl:text>&nbsp;|&nbsp;</xsl:text>

	    <!-- ============== Next Page ============== -->
	    <xsl:choose>

	      <xsl:when test="$nextStartRecord &lt;= $lastPageStartRecord">
		<span class="active">
		  <a href="{$sru_searchRetrieveURL}&amp;startRecord={$nextStartRecord}">
		    <xsl:text>Next&nbsp;</xsl:text>
		    <xsl:value-of select="$maximumRecords"/>
		  </a>
		</span>
	      </xsl:when>

	      <xsl:otherwise>
		<span class="inactive">
		  <xsl:text>Next </xsl:text><xsl:value-of select="$maximumRecords"/>
		</span>
	      </xsl:otherwise>

	    </xsl:choose>
	    <!-- ======================================= -->

	    <xsl:text>&nbsp;|&nbsp;</xsl:text>

	    <!-- ============== Last Page ============== -->
	    <xsl:choose>

	      <xsl:when test="$startRecord != $lastPageStartRecord">
		<span class="active">
		  <a href="{$sru_searchRetrieveURL}&amp;startRecord={$lastPageStartRecord}">
		    <xsl:text>Last page</xsl:text>
		  </a>
		</span>
	      </xsl:when>

	      <xsl:otherwise>
		<span class="inactive">
		  <xsl:text>Last page</xsl:text>
		</span>
	      </xsl:otherwise>

	    </xsl:choose>
	    <!-- ======================================= -->

	</xsl:if>

	</td>
      </tr>
    </table>
  </xsl:template>
</xsl:stylesheet>
