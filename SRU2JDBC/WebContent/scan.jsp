<%@page contentType="text/xml"%><?xml version="1.0" encoding='UTF-8'?><%@page import="org.wmo.jzkitsru2jdbc.servlets.WMOSRU"%><%@page import="java.util.Collection"%>
<sru:scanResponse xmlns:sru="http://www.loc.gov/zing/srw/"
	                xmlns:zr="http://explain.z3950.org/dtd/2.0/"
                  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

	<sru:version><%= request.getAttribute("version") %></sru:version>

  <%@include file="diagnostic.jspf"%>
       
</sru:scanResponse>