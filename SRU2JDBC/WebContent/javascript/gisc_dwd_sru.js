function addClass (element, className) {
	if (!hasClass(element, className)) {
		if (element.className) {
			element.className += " " + className;
		} else {
			element.className = className;
		}
	}
}

function removeClass (element, className) {
	var regexp = addClass[className];
	if (!regexp) {
		regexp = addClass[className] = new RegExp("(^|\\s)" + className + "(\\s|$)");
	}
	element.className = element.className.replace(regexp, "$2");
}

function hasClass (element, className) {
	var regexp = addClass[className];
	if (!regexp) {
		regexp = addClass[className] = new RegExp("(^|\\s)" + className + "(\\s|$)");
	}
	return regexp.test(element.className);
}

function toggleClass (element, className) {
    if (hasClass(element,className)) {
	removeClass(element, className);
	return false;
    } else {
	addClass(element, className);
	return true;
    }
}

function toggleDisclosure(id) {

    // obtain a reference to the desired div
    // if no such table exists, abort
    var disclosureElement = document.getElementById(id);
    if (! disclosureElement) { 
	return; 
    }

    toggleClass(disclosureElement, "closed");
}

function toggleDeepDisclosure(id) {

    // obtain a reference to the desired div
    // if no such table exists, abort
    var disclosureElement = document.getElementById(id);
    if (! disclosureElement) { 
	return; 
    }
    
    var isClosed = toggleClass(disclosureElement, "closed");

    var fieldsets = disclosureElement.getElementsByTagName("fieldset");
    for (var i = 0; i < fieldsets.length; ++i) {
	if (isClosed) {
	    addClass(fieldsets[i], "closed");
	} else {
	    removeClass(fieldsets[i], "closed");
	}
    }
}

function toolTip(id) {
    /* TODO alert(id); TODO */
}
