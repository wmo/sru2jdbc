<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />    
    <title>DWD Search/Retrieval via URL (SRU)</title>
    
    
    <link rel="stylesheet" type="text/css" href="css/GISC_DWD_SRU.css">
    <link rel="stylesheet" type="text/css" href="css/GISC_DWD_SRU_input.css">
       
		<script type="text/javascript">
		  <%@include file="functions.jspf"%>  
		</script>    
  </head>
  
  <body OnLoad="setFocus('queryId');">
    <div id="gisc">
      <%@include file="header.jspf"%>  
      <h2>&nbsp;DWD Search/Retrieval via URL (SRU)</h2>

      <div class="formlist">
	      <form  id="inputFormId"
	             method=POST target="results"
	             accept-charset="UTF-8"
	             action="#"
	             onsubmit="return displayResult(this);">

	        <input id="actionId" type="hidden" name="myAction" value="">
	        
	        <%@include file="input_server.jspf"%>  
	        <%@include file="input_query.jspf"%>  
	        <%@include file="input_params.jspf"%>  
	                        
	      </form>
	                              
	      <p id="completeId" >&nbsp;</p>
  
      </div>
    </div>
  </body>
  
</html>  